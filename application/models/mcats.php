<?php 

class Mcats extends CI_Model{
	
	public $data 	= array();

	
	function __construct(){
		parent::__construct();
	}


	function category_get_children($id, $separator, $counter)
	{
		$this->db->order_by('name', 'asc');
		$query = $this->db->get_where(TBL_CAT, array('parent_id' =>$id));
		if ($query->num_rows() == 0) {

			return FALSE;

		} else {

			foreach ($query->result() as $row) {

				$counter++;
				$this->data[$counter]['id'] = $row->id;
				$this->data[$counter]['parent_id'] = $row->parent_id;
				$this->data[$counter]['name'] = $separator.$row->name;
				$this->data[$counter]['slug'] = $row->slug;
				$this->data[$counter]['real_name'] = $row->name;
				$children = $this->category_get_children($row->id, $separator.' - ', $counter);

				if ($children != FALSE) {
					$counter = $counter + $children;
				}
			}

			return $counter;
		}
	}


	function category_get_all($cat_id = 0)
	{
		$this->data = array();
		$this->db->order_by('name', 'asc');
		$query = $this->db->get_where(TBL_CAT, array('parent_id' => $cat_id));
		$counter = 0;
		foreach ($query -> result() as $row) {
			$this->data[$counter]['id'] = $row->id;
			$this->data[$counter]['parent_id'] = $row->parent_id;
			$this->data[$counter]['name'] = $row->name;
			$this->data[$counter]['slug'] = $row->slug;
			$this->data[$counter]['real_name'] = $row->name;

			$children = $this->category_get_children($row->id, ' - ', $counter);
			$counter = $counter + $children;
			$counter++;
		}
		return $this->data;
	}

	function category_get_all_parent($id, $counter)
	{
		$row = $this->db->get_where(TBL_CAT, array('id'=> $id))->row_array();
		$this->data[$counter] = $row;
		if ($row['parent_id'] != 0) {
			$counter++;
			$this->category_get_all_parent($row['parent_id'], $counter);
		}
		return array_reverse($this->data);

	}

	
}

?>