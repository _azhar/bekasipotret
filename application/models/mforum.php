<?php 

class Mforum extends CI_Model{
			
	
	function __construct(){
		parent::__construct();
	}

	function get_all($start, $limit){

		 $sql = "SELECT a.*, b.name as category_name, b.slug as category_slug, c.date_add 
                FROM ".TBL_THREAD." a, ".TBL_CAT." b, ".TBL_POST." c 
                WHERE a.category_id = b.id AND a.id = c.thread_id 
                AND c.date_add = (SELECT MAX(date_add) FROM ".TBL_POST." WHERE thread_id = a.id LIMIT 1) 
                ORDER BY c.date_add DESC LIMIT ".$start.", ".$limit;
        return $this->db->query($sql)->result();
	}

	function get_total_by_category($cat_id)
	{
		$cat_string = "(";
			foreach ($cat_id as $key => $id) {
				if ($key == 0) {
					$cat_string .= " a.category_id = ".$id;
				} else {
					$cat_string .= " OR a.category_id = ".$id;
				}
			}

			$cat_string .= ")";

		$sql = "SELECT a.* FROM ".TBL_THREAD." a WHERE ".$cat_string;
        return $this->db->query($sql)->num_rows();
	}

	function get_by_category($start, $limit, $cat_id)
	{
		$cat_string = "(";
			foreach ($cat_id as $key => $id) {
				if ($key == 0) {
					$cat_string .= " a.category_id = ".$id;
				} else {
					$cat_string .= " OR a.category_id = ".$id;
				}
			}
			$cat_string .= ")";
	        $sql = "SELECT a.*, b.name as category_name, b.slug as category_slug, c.date_add 
                FROM ".TBL_THREAD." a, ".TBL_CAT." b, ".TBL_POST." c 
                WHERE a.category_id = b.id AND a.id = c.thread_id AND ".$cat_string." 
                AND c.date_add = (SELECT MAX(date_add) FROM ".TBL_POST." WHERE thread_id = a.id LIMIT 1) 
                ORDER BY c.date_add DESC LIMIT ".$start.", ".$limit;
        return $this->db->query($sql)->result();
	}
}

?>