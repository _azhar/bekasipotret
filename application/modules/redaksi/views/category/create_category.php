			<div id="content" class="span10">
			<!-- content starts -->

				<div>
					<ul class="breadcrumb">
						<li>
							News Forum <span class="divider">/</span>
						</li>
						<li>
							<a href="<?php echo base_url();?>redaksi/redaksi_main/lihat_berita">Lihat Berita</a>
						</li>
					</ul>
				</div>

			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Create Category</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
				   <?php if (isset($error)): ?>
			        <div class="alert alert-error">
			            <a class="close" data-dismiss="alert" href="#">&times;</a>
			            <h4 class="alert-heading">Error!</h4>
			   			<?php if (isset($error['name'])): ?>
                		<div>- <?php echo $error['name']; ?></div>
	           		 	<?php endif; ?>
	            		<?php if (isset($error['slug'])): ?>
	                	<div>- <?php echo $error['slug']; ?></div>
	            		<?php endif; ?>  
	        		</div>
	        		<?php endif; ?>  
	        		<?php if (isset($tmp_success)): ?>
	        		 <div class="alert alert-success">
           				 <a class="close" data-dismiss="alert" href="#">&times;</a>
           				 <h4 class="alert-heading">New category added!</h4>
       				 </div>
       				<?php endif; ?>
					<div class="box-content">
						<form class="form-horizontal" action="" method="post">
						  <fieldset>
							<legend>New Category</legend>
						     <script>
					        $(function() {
					            $('#name').change(function() {
					                var name = $('#name').val().toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
					                $('#slug').val(name);
					            });
					        });
					        </script>
							  <div class="control-group">
								<label class="control-label" for="input01"> Name </label>
								<div class="controls">
								<input type="text" id="title" name="row[name]" class="input-xlarge">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label" for="input01"> Slug </label>
								<div class="controls">
								<input type="text" id="slug" name="row[slug]" class="input-xlarge">
								</div>
							  </div>
							<div class="control-group">
								<label class="control-label" for="selectError">Parent Category</label>
									<div class="controls">
								  <select id="selectError" name="row[parent_id]" data-rel="chosen">
								  	<option value="0">-- none --</option> 
									<?php foreach ($cats as $cat): ?>
									<option value="<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></option>
									<?php endforeach; ?>
								  </select>
								</div>
							  </div>
							<div class="form-actions">
							  <button type="submit" name="btn-create" value="Create Thread" class="btn btn-primary">Save changes</button>
							  <button type="reset" class="btn" >Cancel</button>
							</div>
						  </fieldset>
						</form>   

					</div>
				</div><!--/span-->

			</div><!--/row-->	
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
			</div>
