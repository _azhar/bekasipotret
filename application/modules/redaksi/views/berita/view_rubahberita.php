<<<<<<< HEAD
			<div id="content" class="span10">
			<!-- content starts -->

				<div>
					<ul class="breadcrumb">
						<li>
							Berita <span class="divider">/</span>
						</li>
						<li>
							<a href="<?php echo base_url();?>redaksi/redaksi_main/status_berita">Status Berita</a> <span class="divider">/</span>
						</li>
						<li>
							Rubah Berita
						</li>
					</ul>
				</div>

				<div class="row-fluid sortable">		
				<div class="box span8">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Rubah Berita</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php foreach ($getberita_byid->result() as $row) { ?>
						<?php
							$act = base_url().'redaksi/redaksi_main/rubah_berita/'.$row->id;
							$att = array(
								'class' => 'form-horizontal'
								);
							?>
						<?php echo form_open($act,$att); ?>
							<fieldset>
									<div class="control-group">
										<label class="control-label" for="kategori">Kategori</label>
										<div class="controls">
											<input class="input-xlarge uneditable-input" id="kategori" name="kategori" type="text" value="<?php echo $row->category_id;?>">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="judul">Judul Berita</label>
										<div class="controls">
											<input class="input-xlarge focused" id="judul" name="judul" type="text" value="<?php echo $row->title;?>">
										</div>
									</div>
						<?php } ?>
								
								<div class="control-group">
									<label class="control-label" for="isi">Isi Berita</label>
									<div class="controls">
										<textarea class="cleditor" id="isi" name="isi" rows="3"></textarea>
									</div>
								</div>
								<div class="control-group">
									<div class="controls">
										<div class="<?php echo $alert;?>">
											<strong><?php echo $comment;?></strong>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" name="mysubmit" id="mysubmit" class="btn btn-primary">Simpan</button>
									<button type="reset" class="btn">Batal</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div><!--/span-->

				<div class="box span4">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Upload Gambar</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php foreach ($getberita_byid->result() as $row) { ?>
						<?php
							$act = base_url().'redaksi/redaksi_main/upload_gambar/'.$row->id;
							$att = array(
								'class' => 'form-horizontal'
								);
							?>
						<?php } ?>
						<?php echo form_open_multipart($act,$att); ?>
							<fieldset>
							<?php foreach ($getimage->result() as $row) { ?>
								<div class="control-group">
									<img width="150" class="grayscale" src="<?php echo base_url().'uploads/'.$row->image; ?>" alt="<?php echo $row->image; ?>">
									<a class="btn btn-mini btn-danger" href="<?php echo base_url().'redaksi/redaksi_main/delete_gambar/'.$row->id; ?>">
										<i class="icon-trash icon-white"></i>  Hapus
									</a>
								</div>
							<?php }?>
								<div class="control-group">
									<div class="uploader" id="uniform-fileInput">
										<input type="file" id="fileInput" name="fileInput" class="input-file uniform_on" size="19" style="opacity: 0;">
										<span class="filename" style="-moz-user-select: none;">No file selected</span>
										<span class="action" style="-moz-user-select: none;">Choose File</span>
									</div>
								</div>
								<div class="control-group">
									<div class="well">
										<button type="submit" name="mysubmit" id="mysubmit" class="btn btn-primary">Upload</button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

			<!-- content ends -->
=======
			<div id="content" class="span10">
			<!-- content starts -->

				<div>
					<ul class="breadcrumb">
						<li>
							Berita <span class="divider">/</span>
						</li>
						<li>
							<a href="<?php echo base_url();?>redaksi/redaksi_main/status_berita">Status Berita</a> <span class="divider">/</span>
						</li>
						<li>
							Rubah Berita
						</li>
					</ul>
				</div>

				<div class="row-fluid sortable">		
				<div class="box span8">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Rubah Berita</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php foreach ($getberita_byid->result() as $row) { ?>
						<?php
							$act = base_url().'redaksi/redaksi_main/rubah_berita/'.$row->id;
							$att = array(
								'class' => 'form-horizontal'
								);
							?>
						<?php echo form_open($act,$att); ?>
							<fieldset>
									<div class="control-group">
										<label class="control-label" for="kategori">Kategori</label>
										<div class="controls">
											<input class="input-xlarge uneditable-input" id="kategori" name="kategori" type="text" value="<?php echo $row->category_id;?>">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label" for="judul">Judul Berita</label>
										<div class="controls">
											<input class="input-xlarge focused" id="judul" name="judul" type="text" value="<?php echo $row->title;?>">
										</div>
									</div>
						<?php } ?>
								
								<div class="control-group">
									<label class="control-label" for="isi">Isi Berita</label>
									<div class="controls">
										<textarea class="cleditor" id="isi" name="isi" rows="3"></textarea>
									</div>
								</div>
								<div class="control-group">
									<div class="controls">
										<div class="<?php echo $alert;?>">
											<strong><?php echo $comment;?></strong>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" name="mysubmit" id="mysubmit" class="btn btn-primary">Simpan</button>
									<button type="reset" class="btn">Batal</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div><!--/span-->

				<div class="box span4">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Upload Gambar</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php foreach ($getberita_byid->result() as $row) { ?>
						<?php
							$act = base_url().'redaksi/redaksi_main/upload_gambar/'.$row->id;
							$att = array(
								'class' => 'form-horizontal'
								);
							?>
						<?php } ?>
						<?php echo form_open_multipart($act,$att); ?>
							<fieldset>
							<?php foreach ($getimage->result() as $row) { ?>
								<div class="control-group">
									<img width="150" class="grayscale" src="<?php echo base_url().'uploads/'.$row->image; ?>" alt="<?php echo $row->image; ?>">
									<a class="btn btn-mini btn-danger" href="<?php echo base_url().'redaksi/redaksi_main/delete_gambar/'.$row->id; ?>">
										<i class="icon-trash icon-white"></i>  Hapus
									</a>
								</div>
							<?php }?>
								<div class="control-group">
									<div class="uploader" id="uniform-fileInput">
										<input type="file" id="fileInput" name="fileInput" class="input-file uniform_on" size="19" style="opacity: 0;">
										<span class="filename" style="-moz-user-select: none;">No file selected</span>
										<span class="action" style="-moz-user-select: none;">Choose File</span>
									</div>
								</div>
								<div class="control-group">
									<div class="well">
										<button type="submit" name="mysubmit" id="mysubmit" class="btn btn-primary">Upload</button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

			<!-- content ends -->
>>>>>>> d2427ac0ddca3a1dd766e87bcd2cd8cfac60d0a6
			</div><!--/span-->