			<div id="content" class="span10">
			<!-- content starts -->
<?php if (isset($tmp_success_del)): ?>
    <div class="alert alert-info">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        <h4 class="alert-heading">User deleted!</h4>
    </div>
    <?php endif; ?>
				<div>
					<ul class="breadcrumb">
						<li>
							News Forum <span class="divider">/</span>
						</li>
						<li>
							<a href="<?php echo base_url();?>redaksi/redaksi_main/lihat_berita">Lihat Berita</a>
						</li>
					</ul>
				</div>

			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					<i class="icon-list-alt"></i>
					</div>
			
			<?php if (count($post)) {
				echo "
				<div class='box-content'>
					<table class='table table-striped table-bordered bootstrap-datatable datatable'>
						<thead>
					 	  <tr>
					 	  	<th>No</th>
						  	<th>Nama</th>
						  	<th>Post</th>
							<th>Action</th>
					   	  </tr>
						</thead>   
					<tbody>";
				

				   foreach ($post as $kk => $key) {
				   	$kk =$kk+1;
				   	$nama = $this->madmin->get_id($key['author_id']);

				   	echo "<tr>";
				    echo "<td class='center'>".$kk."</td>";

				   	foreach ($nama as $ss => $lala) {
				   		echo "<td class='center'><a href='#'>".$lala['username']."</a></td>";
				   	}
				    echo "<td class='center'><a href='#'>".$key['post']."</a></td>";
				    echo "<td class='center'>";
				    	echo "<a class='btn btn-success' href=".site_url('redaksi/redaksi_main/edit_post')."/".$key['id'].">";
						echo "<i class='icon-edit icon-white'></i>";
				    	echo "Edit</a>";
				    	echo "<a class='btn btn-danger' href=".site_url('redaksi/redaksi_main/delete_post')."/".$key['id'].">";
				    	echo "<i class='icon-trash icon-white'></i>";
				    	echo "Delete</a>";
				    echo "</td>";
				    echo "</tr>";
						  }
						 

			echo " 
						  </tbody>
					  </table>
				</div>
			</div>";
				} ?>

			</div><!--/#content.span10-->
				</div><!--/fluid-row-->

		<hr>							
	</div>