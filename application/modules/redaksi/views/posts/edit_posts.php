			<div id="content" class="span10">
			<!-- content starts -->

				<div>
					<ul class="breadcrumb">
						<li>
							News Forum <span class="divider">/</span>
						</li>
						<li>
							<a href="<?php echo base_url();?>redaksi/redaksi_main/lihat_berita">Lihat Berita</a>
						</li>
					</ul>
				</div>
				
			<?php if (isset($tmp_success)): ?>
	        		 <div class="alert alert-success">
           				 <a class="close" data-dismiss="alert" href="#">&times;</a>
           				 <h4 class="alert-heading">New category added!</h4>
       				 </div>
       				<?php endif; ?>
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Create Category</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
				   <?php if (isset($error)): ?>
			        <div class="alert alert-error">
			            <a class="close" data-dismiss="alert" href="#">&times;</a>
			            <h4 class="alert-heading">Error!</h4>
			   			<?php if (isset($error['post'])): ?>
                		<div>- <?php echo $error['post']; ?></div>
	           		 	<?php endif; ?>
	        		</div>
	        		<?php endif; ?>  
	        		
					<div class="box-content">
						<form class="form-horizontal" action="" method="post">
							 
						  <fieldset>
							<legend>Edit Posts</legend>
						     <script>
					        $(function() {
					            $('#name').change(function() {
					                var name = $('#name').val().toLowerCase().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
					                $('#slug').val(name);
					            });
					        });
					        </script>
					        <input type="hidden" name="row[id]" value="<?php echo $post->id; ?>"/>
						 	<div class="control-group">
							  <label class="control-label" for="textarea2">Body Post</label>
							  <div class="controls">
								<textarea class="cleditor"  name="row[post]" rows="3"><?php echo $post->post; ?></textarea>
							  </div>
							</div>
							<div class="form-actions">
							  <button type="submit" name="btn-edit" value="Save" class="btn btn-primary">Save changes</button>
							  <button type="reset" class="btn" >Cancel</button>
							</div>
						  </fieldset>
						</form>   

					</div>
				</div><!--/span-->

			</div><!--/row-->	
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->

			</div>