<<<<<<< HEAD
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_main extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function getall_user(){
		$query = $this->db->get('users');
		return $query;
	}

	function getall_berita(){
		$this->db->order_by('pubdate', 'desc');
		$query = $this->db->get('post');
		return $query;
	}

	function getall_beritabaru(){
		$this->db->order_by('pubdate', 'desc');
		$this->db->where('status', 'baru');
		$query = $this->db->get('post');
		return $query;
	}

	function getpewarta_byid($id){
		$this->db->where('id_pewarta', $id);
		$query = $this->db->get('pewarta');
		return $query;
	}

	function getcategory_byid($id){
		$this->db->where('id', $id);
		$query = $this->db->get('kategori');
		return $query;
	}

	function getall_kategori(){
		$this->db->order_by('kategori', 'asc');
		$query = $this->db->get('kategori');
		return $query;
	}

	function add_kategori(){
		$data = array(
			'id' => NULL,
			'kategori' => $this->input->post('tambahkategori')
			);
		$this->db->insert('kategori', $data);
	}

	function delete_kategori($id){
		$this->db->where('id', $id);
		$this->db->delete('kategori');
	}

	function update_kategori($id){
		$data = array(
			'kategori' => $this->input->post('kategori')
			);
		$this->db->where('id', $id);
		$this->db->update('kategori',$data);
	}

	function insert_post(){
		$kategori = $this->input->post('kategori');
		$this->db->select('id');
		$this->db->where('kategori', $kategori);
		$kategori_id = $this->db->get('kategori')->row_array();

		$data = array (
			'id' => NULL,
			'title' => $this->input->post('judul'),
			'tags' => NULL,
			'status' => 'Baru',
			'body' => $this->input->post('isi'),
			'category_id' => $kategori_id['id'],
			'pubdate' => date('Y-m-d H:i:s'),
			'user_id' => $this->tank_auth->get_user_id(),
			'teraktual' =>'N',
			'link' => '#',
			'inspiratif' =>'N',
			'bermanfaat' =>'N',
			'menarik' =>'N'
			);
		$this->db->insert('post',$data);
	}

	function update_post($id){
		$data = array(
			'id' => $id,
			'title' => $this->input->post('judul'),
			'tags' => NULL,
			'status' => 'Baru',
			'body' => $this->input->post('isi'),
			'category_id' => $this->input->post('kategori'),
			'pubdate' => date('Y-m-d H:i:s'),
			'user_id' => $this->tank_auth->get_user_id(),
			'teraktual' =>'N',
			'link' => '#',
			'inspiratif' =>'N',
			'bermanfaat' =>'N',
			'menarik' =>'N'
			);
		$this->db->where('id', $id);
		$this->db->update('post',$data);
	}

	function upload_image($id,$filename){
		$data = array(
			'id' => NULL,
			'id_berita' => $id,
			'image' => $filename
			);
		$this->db->insert('image',$data);
	}

	function delete_image($id){
		$this->db->where('id', $id);
		$this->db->delete('image');
	}

	function getimage_byid($id){
		$this->db->where('id', $id);
		$query = $this->db->get('image');
		return $query;
	}

	function status_berita($id,$status){
		$data = array(
			'status' => $status
			);
		$this->db->where('id', $id);
		$this->db->update('post', $data);
	}

	function getberita_byid($id){
		$this->db->where('id', $id);
		$query = $this->db->get('post');
		return $query;
	}

	function getimage($id_berita){
		$this->db->where('id_berita', $id_berita);
		$query = $this->db->get('image');
		return $query;
	}

	function getberita_bypewarta($user_id){
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('post');
		return $query;
	}

	function getall_pewarta(){
		$query = $this->db->get('pewarta');
		return $query;
	}

}

/* End of file model_main.php */
/* Location: ./application/modules/redaksi/models/model_main.php */
=======
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_main extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function getall_user(){
		$query = $this->db->get('users');
		return $query;
	}

	function getall_berita(){
		$this->db->order_by('pubdate', 'desc');
		$query = $this->db->get('post');
		return $query;
	}

	function getall_beritabaru(){
		$this->db->order_by('pubdate', 'desc');
		$this->db->where('status', 'baru');
		$query = $this->db->get('post');
		return $query;
	}

	function getpewarta_byid($id){
		$this->db->where('id_pewarta', $id);
		$query = $this->db->get('pewarta');
		return $query;
	}

	function getcategory_byid($id){
		$this->db->where('id', $id);
		$query = $this->db->get('kategori');
		return $query;
	}

	function getall_kategori(){
		$this->db->order_by('kategori', 'asc');
		$query = $this->db->get('kategori');
		return $query;
	}

	function add_kategori(){
		$data = array(
			'id' => NULL,
			'kategori' => $this->input->post('tambahkategori')
			);
		$this->db->insert('kategori', $data);
	}

	function delete_kategori($id){
		$this->db->where('id', $id);
		$this->db->delete('kategori');
	}

	function update_kategori($id){
		$data = array(
			'kategori' => $this->input->post('kategori')
			);
		$this->db->where('id', $id);
		$this->db->update('kategori',$data);
	}

	function insert_post(){
		$kategori = $this->input->post('kategori');
		$this->db->select('id');
		$this->db->where('kategori', $kategori);
		$kategori_id = $this->db->get('kategori')->row_array();

		$data = array (
			'id' => NULL,
			'title' => $this->input->post('judul'),
			'tags' => NULL,
			'status' => 'Baru',
			'body' => $this->input->post('isi'),
			'category_id' => $kategori_id['id'],
			'pubdate' => date('Y-m-d H:i:s'),
			'user_id' => $this->tank_auth->get_user_id(),
			'teraktual' =>'N',
			'link' => '#',
			'inspiratif' =>'N',
			'bermanfaat' =>'N',
			'menarik' =>'N'
			);
		$this->db->insert('post',$data);
	}

	function update_post($id){
		$data = array(
			'id' => $id,
			'title' => $this->input->post('judul'),
			'tags' => NULL,
			'status' => 'Baru',
			'body' => $this->input->post('isi'),
			'category_id' => $this->input->post('kategori'),
			'pubdate' => date('Y-m-d H:i:s'),
			'user_id' => $this->tank_auth->get_user_id(),
			'teraktual' =>'N',
			'link' => '#',
			'inspiratif' =>'N',
			'bermanfaat' =>'N',
			'menarik' =>'N'
			);
		$this->db->where('id', $id);
		$this->db->update('post',$data);
	}

	function upload_image($id,$filename){
		$data = array(
			'id' => NULL,
			'id_berita' => $id,
			'image' => $filename
			);
		$this->db->insert('image',$data);
	}

	function delete_image($id){
		$this->db->where('id', $id);
		$this->db->delete('image');
	}

	function getimage_byid($id){
		$this->db->where('id', $id);
		$query = $this->db->get('image');
		return $query;
	}

	function status_berita($id,$status){
		$data = array(
			'status' => $status
			);
		$this->db->where('id', $id);
		$this->db->update('post', $data);
	}

	function getberita_byid($id){
		$this->db->where('id', $id);
		$query = $this->db->get('post');
		return $query;
	}

	function getimage($id_berita){
		$this->db->where('id_berita', $id_berita);
		$query = $this->db->get('image');
		return $query;
	}

	function getberita_bypewarta($user_id){
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('post');
		return $query;
	}

	function getall_pewarta(){
		$query = $this->db->get('pewarta');
		return $query;
	}

}

/* End of file model_main.php */
/* Location: ./application/modules/redaksi/models/model_main.php */
>>>>>>> d2427ac0ddca3a1dd766e87bcd2cd8cfac60d0a6
