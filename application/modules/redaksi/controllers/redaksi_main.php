
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redaksi_main extends CI_Controller {
	public $data         = array();
    public $page_config  = array();
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('check_role');
		$this->load->model('usermodel');
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		$this->load->library('tank_auth_groups','','tank_auth');
		$this->load->model('model_main');
		$this->load->model('mthread');
		$this->load->model('mposts');
		$this->load->model('madmin');
		$topbar['username'] = $this->tank_auth->get_username();
		$head['menu_head'] = $this->usermodel->menu_head();
		$this->data['head'] = $this->load->view('head', '', TRUE);
		$this->data['topbar'] = $this->load->view('topbar', $topbar, TRUE);
		$this->data['leftmenu'] = $this->load->view('leftmenu', $head, TRUE);
		$this->data['footer'] = $this->load->view('footer', '', TRUE);
		$this->data['javascript'] = $this->load->view('javascript', '', TRUE);
	}

	function index(){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getall_user'] = $this->model_main->getall_user();
			$content['getall_berita'] = $this->model_main->getall_berita();
			$this->data['content'] = $this->load->view('view_home', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
		
	}

	function buat_baru(){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getall_kategori'] = $this->model_main->getall_kategori();

			$this->form_validation->set_rules('judul','Judul','required');
			$this->form_validation->set_rules('isi','Isi','required');

			$content['comment'] = '';
			$content['alert'] = '';

			if ($this->form_validation->run()==TRUE) {

				$content['comment'] = 'input berhasil';
				$content['alert'] = 'alert alert-success';

				$this->model_main->insert_post();
			}

			$this->data['content'] = $this->load->view('berita/view_buatbaru', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function lihat_berita(){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getall_berita'] = $this->model_main->getall_berita();
			$this->data['content'] = $this->load->view('berita/view_lihatberita', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function tampilkan_berita($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getberita_byid'] = $this->model_main->getberita_byid($id);
			$this->data['content'] = $this->load->view('berita/view_tampilkanberita', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function status_berita(){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getall_beritabaru'] = $this->model_main->getall_beritabaru();
			$this->data['content'] = $this->load->view('berita/view_statusberita', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function persetujuan_berita($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getberita_byid'] = $this->model_main->getberita_byid($id);
			$this->data['content'] = $this->load->view('berita/view_persetujuanberita', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function rubah_berita($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getberita_byid'] = $this->model_main->getberita_byid($id);
			$content['getimage'] = $this->model_main->getimage($id);

			$this->form_validation->set_rules('judul','Judul','required');
			$this->form_validation->set_rules('isi','Isi','required');

			$content['comment'] = '';
			$content['alert'] = '';

			if ($this->form_validation->run()==TRUE) {

				$content['comment'] = 'update berhasil';
				$content['alert'] = 'alert alert-success';

				$this->model_main->update_post($id);
			}

			$this->data['content'] = $this->load->view('berita/view_rubahberita', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function upload_gambar($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'jpg|png|gif';

			$this->load->library('upload',$config);
			$this->upload->do_upload('fileInput');

			$upload_data = $this->upload->data();
			$filename = $upload_data['file_name'];

			if ($filename!=null) {
				$this->model_main->upload_image($id,$filename);
			}
			
			redirect('/redaksi/redaksi_main/rubah_berita/'.$id);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function delete_gambar($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$image_id = $this->model_main->getimage_byid($id);
			foreach ($image_id->result() as $row) {
				$id_berita = $row->id_berita;
				unlink('./uploads/'.$row->image);
			}

			$this->model_main->delete_image($id);
			redirect('/redaksi/redaksi_main/rubah_berita/'.$id_berita);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function setuju($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

			$status='disetujui';
			$this->model_main->status_berita($id,$status);

			$content['getberita_byid'] = $this->model_main->getberita_byid($id);
			$this->data['content'] = $this->load->view('berita/view_tampilkanberita', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function tolak($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

			$status='ditolak';
			$this->model_main->status_berita($id,$status);

			$content['getberita_byid'] = $this->model_main->getberita_byid($id);
			$this->data['content'] = $this->load->view('berita/view_tampilkanberita', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function berita_saya(){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$user_id = $this->tank_auth->get_user_id();
			$content['getberita_bypewarta'] = $this->model_main->getberita_bypewarta($user_id);
			$this->data['content'] = $this->load->view('berita/view_beritasaya', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function kategori(){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getall_kategori'] = $this->model_main->getall_kategori();
			$this->data['content'] = $this->load->view('kategori_redaksi/view_kategori', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function tambah_kategori(){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

			$this->model_main->add_kategori();

			redirect('/redaksi/redaksi_main/kategori/');
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function hapus_kategori($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

			$this->model_main->delete_kategori($id);

			redirect('/redaksi/redaksi_main/kategori/');
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function rubah_kategori($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getcategory_byid'] = $this->model_main->getcategory_byid($id);
			$this->data['content'] = $this->load->view('kategori_redaksi/view_rubahkategori', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function update_kategori($id){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

			$this->model_main->update_kategori($id);

			redirect('/redaksi/redaksi_main/kategori/');
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function pewarta(){
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
			$content['getall_pewarta'] = $this->model_main->getall_pewarta();
			$this->data['content'] = $this->load->view('view_pewarta', $content, TRUE);

			$this->load->view('view_redaksi_main',$this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function news()
	{

		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated
		
			$content['get_all_posts'] = $this->mthread->get_thread_all();
			$this->data['content'] = $this->load->view('news/home_news', $content, TRUE);

			$this->load->view('view_redaksi_main', $this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}
	function create_news()
	{
		//Set cek, check for thread_create on models check role
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($this->tank_auth->is_logged_in(TRUE)) {	  // logged in, activated
			
			// cek thread_create - if !=1 to news
			if ($cek['thread_create'] != 1 ) {
				redirect('redaksi/redaksi_main/news');
			}

			// create thread			
			if ($this->input->post('btn-create')) {
            
            	$this->mthread->create();
            	if ($this->mthread->error_count != 0) {
                $content['error']    = $this->mthread->error;
            	
            	} else {
                
                $this->session->set_userdata('tmp_success_new', 1);
                redirect('forum/talk/'.$this->mthread->fields['slug']);
            	
            	  } 
            }

            $content['cats'] = $this->mposts->category_get_all();
			$content['get_all_posts'] = $this->mthread->get_thread_all();
			$this->data['content'] = $this->load->view('news/create_news', $content, TRUE);
			$this->load->view('view_redaksi_main', $this->data);
		}

		elseif ($this->tank_auth->is_logged_in(FALSE)) { // logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}

		else {
			redirect('/redaksi/auth/');
		}
	}

	function news_edit($thread_id)
	{
		//Set cek, check for thread_edit on models check role
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($this->tank_auth->is_logged_in(TRUE)) {	// logged in, activated
			
			// cek thread_create - if !=1 to news
			if ($cek['thread_edit'] != 1 ) {
				redirect('redaksi/redaksi_main/news');
			
			}
			
			// create thread
			if ($this->input->post('btn-save')) {
            	$this->madmin->thread_edit();
            
            	if ($this->madmin->error_count != 0) {
            		$content['error']    = $this->madmin->error;
            
            	} else {
                $this->session->set_userdata('tmp_success', 1);
                redirect('redaksi/redaksi_main/news');
     
            	}
        	}

            $content['cats'] = $this->mposts->category_get_all();
			$content['get_all_posts'] = $this->mthread->get_thread_all();
		 	$content['thread']  = $this->db->get_where(TBL_THREAD, array('id' => $thread_id))->row();
			$this->data['content'] = $this->load->view('news/edit_news', $content, TRUE);
			$this->load->view('view_redaksi_main', $this->data);
		
		}

		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated

			redirect('/redaksi/auth/send_again/');

		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function news_delete($thread_id)
	{	
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($cek['thread_delete'] != 1 ) {
			redirect('redaksi/redaksi_main/news');
		}
		 // delete thread
        $this->db->delete(TBL_THREAD, array('id'=> $thread_id));

        // delete all pos on the thread

        $this->db->delete(TBL_POST, array('thread_id'=>$thread_id));
        $this->session->set_userdata('tmp_success_del',1);
        redirect('redaksi/redaksi_main/news');

	}

	function category()
	{	
		if ($this->tank_auth->is_logged_in(TRUE)) {	// logged in, activated

			$tmp_success_del = $this->session->userdata('tmp_success_del');

        if ($tmp_success_del != NULL) {
            // role deleted
            $this->session->unset_userdata('tmp_success_del');
            $content['tmp_success_del'] = 1;
        }

			$content['categories'] = $this->madmin->category_get_all();
			$this->data['content'] = $this->load->view('category/home_category', $content, TRUE);

			$this->load->view('view_redaksi_main', $this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function create_category()
	{	
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($this->tank_auth->is_logged_in(TRUE)) {	 // logged in, activated

									
			if ($cek['category_create'] != 1) {
				redirect('redaksi/redaksi_main/category');
			} 
			
				if ($this->input->post('btn-create')) {
            		$this->madmin->category_create();
            
            	if ($this->madmin->error_count != 0) {	
               	 	$content['error']    = $this->madmin->error;
            
	            	} else {
	            
	                $this->session->set_userdata('tmp_success', 1);
	            
	                redirect('redaksi/redaksi_main/category');
            
           		 	}	
        	  }

			$content['cats'] = $this->madmin->category_get_all();
			$this->data['content'] = $this->load->view('category/create_category', $content, TRUE);

			$this->load->view('view_redaksi_main', $this->data);

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {	// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		
		} else {
			redirect('/redaksi/auth/');
		}
	}

	function edit_category($cat_id)
	{
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);
	
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

			if ($cek['category_edit'] != 1) {
				redirect('redaksi/redaksi_main/category');

			}
			
			if ($this->input->post('btn-edit')) {
            	$this->madmin->category_edit();
            
	            if ($this->madmin->error_count != 0) {
	                $content['error']    = $this->madmin->error;
	            
	            } else {
	                $this->session->set_userdata('tmp_success', 1);
	                redirect('redaksi/redaksi_main/edit_category/'.$cat_id);
	            
	            }
        }

        $tmp_success = $this->session->userdata('tmp_success');
        if ($tmp_success != NULL) {
            // new category created
            $this->session->unset_userdata('tmp_success');
            $content['tmp_success'] = 1;

        }

			$content['cats'] = $this->madmin->category_get_all();
			$content['category'] = $this->db->get_where(TBL_CAT, array('id' => $cat_id))->row();
			$this->data['content'] = $this->load->view('category/edit_category', $content, TRUE);
			$this->load->view('view_redaksi_main', $this->data);
		
		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		} else { 
			redirect('/redaksi/auth/');
		}
	}
	
	function delete_category($cat_id)
	{
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($cek['category_delete'] != 1) {
			redirect('redaksi/redaksi_main/category');

		}
	
		$this->db->delete(TBL_CAT, array('id'=> $cat_id));
        $this->session->set_userdata('tmp_success_del',1);
        redirect('redaksi/redaksi_main/category');

	}

	function view_role()
	{

		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

			$tmp_success_del = $this->session->userdata('tmp_success_del');
	
			if ($tmp_success_del != NULL) {
             // role deleted
        		$this->session->unset_userdata('tmp_success_del');
        		$content['tmp_success_del'] = 1;
      		 }

      	$content['roles'] = $this->madmin->role_get_all();
		$content['column_width'] = floor(100/count($content['roles']));
		$this->data['content'] = $this->load->view('roles/view_role', $content, TRUE);
		$this->load->view('view_redaksi_main', $this->data);

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		
		} else {
			redirect('/redaksi/auth/');
		}
	}

	function role_edit($role_id)
	{
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

		if ($cek['role_edit'] != 1) {
			redirect('redaksi/redaksi_main/category');

		}
			if ($this->input->post('btn-edit')) {
            $this->madmin->role_edit();
            if ($this->madmin->error_count != 0) {                
                $content['error'] = $this->madmin->error;
            } else {
                $this->session->set_userdata('tmp_success', 1);
                redirect('redaksi/redaksi_main/role_edit/'.$role_id);
            }
        }

        $tmp_success = $this->session->userdata('tmp_success');
        if ($tmp_success != NULL) {
            // new category created
            $this->session->unset_userdata('tmp_success');
            $content['tmp_success'] = 1;
        }
			$content['role'] = $this->db->get_where(TBL_ROLES, array('id' => $role_id))->row();
			$this->data['content'] = $this->load->view('roles/role_edit', $content, TRUE);
			$this->load->view('view_redaksi_main', $this->data);
		
		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		
		} else {
			redirect('/redaksi/auth/');
		}
	}

	function role_create()
	{
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($this->tank_auth->is_logged_in(TRUE)) {	 // logged in, activated
			if ($cek['category_create'] != 1) {
				redirect('redaksi/redaksi_main/view_role');
			}

			if ($this->input->post('btn-create')) {
	            $this->madmin->role_create();

    	        if ($this->madmin->error_count != 0) {
        	        $this->data['error']    = $this->madmin->error;

	            } else {
    	            $this->session->set_userdata('tmp_success', 1);
        	        redirect('redaksi/redaksi_main/role_create');
	            }
        	}

        $tmp_success = $this->session->userdata('tmp_success');
        if ($tmp_success != NULL) {
            // new role created
            $this->session->unset_userdata('tmp_success');
            $this->data['tmp_success'] = 1;
        }

			$this->data['content'] = $this->load->view('roles/role_create','', TRUE);

			$this->load->view('view_redaksi_main', $this->data);

		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		
		} else {
			redirect('/redaksi/auth/');
		}
	}

	function role_delete($role_id)
	{
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($cek['category_delete'] != 1) {
			redirect('redaksi/redaksi_main/view_role');
		}
		$this->db->delete(TBL_ROLES, array('id'=> $role_id));
        $this->session->set_userdata('tmp_success_del', 1);
		redirect('redaksi/redaksi_main/view_role');
	}

	function user_view()
	{
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

		$tmp_success = $this->session->userdata('tmp_success');
        if ($tmp_success != NULL) {
            // user updated
            $this->session->unset_userdata('tmp_success');
            $this->data['tmp_success'] = 1;
        }

        $tmp_success_del = $this->session->userdata('tmp_success_del');
        if ($tmp_success_del != NULL) {
            // user deleted
            $this->session->unset_userdata('tmp_success_del');
            $this->data['tmp_success_del'] = 1;
        }
        	$this->db->order_by('username', 'asc');

      	    $content['users'] = $this->db->get(TBL_USERS)->result();
			$this->data['content'] = $this->load->view('user/user_view', $content, TRUE);

			$this->load->view('view_redaksi_main', $this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function user_delete($user_id)
	{
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($cek['admin_area'] != 1) {
			redirect('redaksi/redaksi_main/user_view');
		}

		$this->db->delete(TBL_USERS, array('id' => $user_id));
        $this->session->set_userdata('tmp_success_del', 1);
        redirect('redaksi/redaksi_main/user_view');
	}

	function post()
	{
		if ($this->tank_auth->is_logged_in(TRUE)) {	// logged in, activated

			$tmp_success_del = $this->session->userdata('tmp_success_del');

        if ($tmp_success_del != NULL) {
            // role deleted
            $this->session->unset_userdata('tmp_success_del');
            $content['tmp_success_del'] = 1;
        }

			$content['post'] = $this->madmin->get_posts();
			$this->data['content'] = $this->load->view('posts/post', $content, TRUE);

			$this->load->view('view_redaksi_main', $this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function mypost()
	{
		$session_data = $this->session->userdata('user_id');
        $id = $session_data['id'];

		if ($this->tank_auth->is_logged_in(TRUE)) {	// logged in, activated

			$tmp_success_del = $this->session->userdata('tmp_success_del');

        if ($tmp_success_del != NULL) {
            // role deleted
            $this->session->unset_userdata('tmp_success_del');
            $content['tmp_success_del'] = 1;
        }

			$content['post'] = $this->madmin->get_posts_id($id);
			$this->data['content'] = $this->load->view('posts/my_post', $content, TRUE);

			$this->load->view('view_redaksi_main', $this->data);
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else{
			redirect('/redaksi/auth/');
		}
	}

	function edit_post($cat_id)
	{

		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);
	
		if ($this->tank_auth->is_logged_in(TRUE)) {								// logged in, activated

			if ($cek['post_edit'] != 1) {
				redirect('redaksi/redaksi_main/post');

			}
			
			if ($this->input->post('btn-edit')) {
            	$this->madmin->edit_post();
            
	            if ($this->madmin->error_count != 0) {
	                $content['error']    = $this->madmin->error;
	            
	            } else {
	                $this->session->set_userdata('tmp_success', 1);
	                redirect('redaksi/redaksi_main/edit_post/'.$cat_id);
	            
	            }
        }

        $tmp_success = $this->session->userdata('tmp_success');
        if ($tmp_success != NULL) {
            // new category created
            $this->session->unset_userdata('tmp_success');
            $content['tmp_success'] = 1;

        }

			$content['cats'] = $this->madmin->category_get_all();
			$content['post'] = $this->db->get_where(TBL_POST, array('id' => $cat_id))->row();
			$this->data['content'] = $this->load->view('posts/edit_posts', $content, TRUE);
			$this->load->view('view_redaksi_main', $this->data);
		
		} elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		} else { 
			redirect('/redaksi/auth/');
		}

	}
		function delete_post($cat_id)
	{
		$idku = $this->session->userdata('group_id');
		$cek = $this->check_role->cek_role($idku);

		if ($cek['post_delete'] != 1) {
			redirect('redaksi/redaksi_main/post');

		}
	
		$this->db->delete(TBL_POST, array('id'=> $cat_id));
        $this->session->set_userdata('tmp_success_del',1);
        redirect('redaksi/redaksi_main/post');
	}
	function change_password()
	{
		if (!$this->tank_auth->is_logged_in()) {								// not logged in or not activated
			redirect('/auth/login/');

		} else {
			$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
			$this->form_validation->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required|xss_clean|matches[new_password]');

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if ($this->tank_auth->change_password(
						$this->form_validation->set_value('old_password'),
						$this->form_validation->set_value('new_password'))) {	// success
					$this->_show_message($this->lang->line('auth_message_password_changed'));

				} else {														// fail
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
				}
			}
			$this->load->view('redaksi/account/change_password_form', $data);
		}
	}

	/**
	 * Change user email
	 *
	 * @return void
	 */
	function change_email()
	{
		if (!$this->tank_auth->is_logged_in()) {								// not logged in or not activated
			redirect('/auth/login/');

		} else {
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');

			$data['errors'] = array();

			if ($this->form_validation->run()) {								// validation ok
				if (!is_null($data = $this->tank_auth->set_new_email(
						$this->form_validation->set_value('email'),
						$this->form_validation->set_value('password')))) {			// success

					$data['site_name'] = $this->config->item('website_name', 'tank_auth');

					// Send email with new email address and its activation link
					$this->_send_email('change_email', $data['new_email'], $data);

					$this->_show_message(sprintf($this->lang->line('auth_message_new_email_sent'), $data['new_email']));

				} else {
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
				}
			}
			$this->load->view('auth/change_email_form', $data);
		}
	}
}
