<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Redaksi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('tank_auth_groups','','tank_auth');
	}

	function index()
	{
		if (!$this->tank_auth->is_logged_in(TRUE)) {							// not logged in, activated
			redirect('/redaksi/auth/login/');
		}
		elseif ($this->tank_auth->is_logged_in(FALSE)) {						// logged in, not activated
			redirect('/redaksi/auth/send_again/');
		}
		else {
			redirect('/redaksi/redaksi_main/');
		}
	}
}

/* End of file redaksi.php */
/* Location: ./application/controllers/redaksi.php */