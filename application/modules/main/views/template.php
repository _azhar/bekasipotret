<?php echo $head; ?>
<body>
	<div id="pagewrap">

		<!-- header web -->
		<?php echo $header; ?>

		<!-- display sidebar -->
		<?php echo $sidebar; ?>

		<!-- display content -->
		<?php echo $content; ?>

		<!-- display footer -->
		<?php echo $footer; ?>

	</div>

</body>
</html>

