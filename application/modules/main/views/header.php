<header id="header">
<hgroup class="logo-header">
	<?php if ($this->session->userdata('user_id') == TRUE) { ?>
		<div id="top-login">
			<h4><a href="<?php base_url();?>redaksi">Dashboard</a></h4>
		</div>
		
		<div id="top-pass">
			<h4><?php echo $this->session->userdata('username'); ?> |</h4>
		</div>
		
	</hgroup>
	<?php } else { ?>

				<div id="top-login">
					<h4><a href="<?php base_url();?>auth/register">Daftar</a></h4>
				</div>
				<div id="top-pass">
					<h4><a href="<?php base_url();?>auth/login">Login |</a></h4>
				</div>
			</hgroup>

			<?php } ?>
	<nav class="clearfix">
		<ul class="clearfix">
			<?php 
			foreach ($menunav->result() as $row) {
				echo "<li><a href='".$row->link."'>".$row->nama_menu."</a></li>";
			}
			?>
		</ul>
		<a href="#" id="pull">Menu</a>
	</nav>
	<form id="searchform">
		<input type="search" id='s' placeholder="search">
	</form>
</header>