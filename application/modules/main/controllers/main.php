<?php if (!defined('BASEPATH')) exit ('No direct script access allowed');

/**
* 
*/
class Main extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();	
		$this->load->model('mposts');
	}

	function index()
	{
		$basurl = base_url().'main/index';			
		$config['base_url'] = $basurl;
		
		$config['total_rows'] = $this->mposts->posting();
		$config['per_page'] = 3;
		
		$config['use_page_numbers'] = TRUE;
		
		$config['first_tag_open'] = '<div>';
		
		$config['first_tag_close'] = '</div>';
		$this->pagination->initialize($config);
		
		$this->db->order_by('pubdate','desc');
		$res = $this->db->get('post', $config['per_page'], $this->uri->segment(3));
		$page['page'] = $res->result_array();

		// load header
		$data['head']=$this->load->view('head',TRUE);

		// load navigation menu
		$menu['menunav']=$this->mposts->menunav();
		$data['header']=$this->load->view('header',$menu,TRUE);


		// sidebar start
		// load widget1 contents
		$widget['teraktual']=$this->mposts->teraktual();
		$widget['inspiratif']=$this->mposts->inspiratif();
		$widget['bermanfaat']=$this->mposts->bermanfaat();
		$widget['menarik']=$this->mposts->menarik();

		// load widget1
		$sidebarcontent['widget1']=$this->load->view('widget1', $widget, TRUE);

		$sidebarcontent['widget2']=$this->load->view('widget2','',TRUE);
		$sidebarcontent['widget3']=$this->load->view('widget3','',TRUE);
		$sidebarcontent['widget4']=$this->load->view('widget4','',TRUE);

		// load sidebar with its content
		$data['sidebar']=$this->load->view('sidebar',$sidebarcontent, TRUE);
		// sidebar end


		// content start
		// load slide of news
		$content['slide_of_news']=$this->load->view('slide_of_news','',TRUE);

		// load recent news
		$content['live_post']=$this->load->view('live_post',$page,TRUE);

		// load content with its content
		$data['content']=$this->load->view('content',$content,TRUE);
		//content end


		// load footer
		$data['footer']=$this->load->view('footer','',TRUE);

		$this->load->view('template',$data);
	}

}