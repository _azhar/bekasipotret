<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or Username';
} else if ($login_by_username) {
	$login_label = 'Username';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>

<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('head'); ?>

<body>
		<div class="container-fluid">
		<div class="row-fluid">
		
			<div class="row-fluid">
				<div class="span12 center login-header">
					<h2>Welcome to Redaksi Potret Bekasi</h2>
				</div><!--/span-->
			</div><!--/row-->
			
			<div class="row-fluid">
				<div class="well span5 center login-box">
					<div class="alert alert-info">
						Please login with your Username and Password.
					</div>
					<form class="form-horizontal" action="<?php echo base_url().$this->uri->uri_string(); ?>" method="post">
						<fieldset>
							<div class="input-prepend" title="Username or Email" data-rel="tooltip">
								<span class="add-on"><i class="icon-user"></i></span><input autofocus class="input-large span10" name="<?php echo $login['id']; ?>" id="<?php echo $login['id']; ?>" type="text" placeholder="<?php echo $login_label; ?>" maxlength="<?php echo $login['maxlength']; ?>" value="<?php echo $login['value']; ?>" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="<?php echo $password['name']; ?>" id="<?php echo $password['id']; ?>" type="password" placeholder="Password" value="" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend">
							<label class="remember" for="<?php echo $remember['id']; ?>"><input type="checkbox" name="<?php echo $remember['name']; ?>" id="<?php echo $remember['id']; ?>" value="<?php echo $remember['value']; ?>" />Remember me</label>
							</div>
							<div class="clearfix"></div>

							<p class="center span5">
							<button type="submit" class="btn btn-primary">Login</button>
							</p>
							<p class="center span5">
								<?php echo anchor('/redaksi/auth/forgot_password/', 'Forgot password'); ?>
								|&nbsp;<?php echo anchor('/redaksi/auth/register/', 'Join Us'); ?>
							</p>
						</fieldset>
					</form>
				</div><!--/span-->
			</div><!--/row-->
				</div><!--/fluid-row-->
		
	</div><!--/.fluid-container-->
<?php $this->load->view('javascript'); ?>
</body>
</html>
