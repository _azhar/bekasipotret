<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum extends CI_Controller {

	public $page_config  = array();
    public $data         = array();

    function __construct()
    {
    	parent::__construct();
    	$this->load->model('mposts');
    	$this->load->model('mforum');
    	$this->load->model('mcats');
        $this->load->model('mthread');
        $this->load->model('check_role');
    }

	public function index($start = 0 )
	{
		    $this->page_config['base_url']    = site_url('forum/index/');
       		$this->page_config['uri_segment'] = 3;
        	$this->page_config['total_rows']  = $this->db->count_all(TBL_THREAD);;
        	$this->page_config['per_page']    = 10;
        
        	$this->set_pagination();
        
        	$this->pagination->initialize($this->page_config);
        	$thread['page']    = $this->pagination->create_links();
        	$menu['menunav']=$this->mposts->menunav();
        	$side['categories']    = $this->mcats->category_get_all();
        	$thread['threads'] = $this->mforum->get_all($start, $this->page_config['per_page']);
        	$data['menunav_view']=$this->load->view('forum/menunav',$menu,TRUE);
			$data['header'] =$this->load->view('forum/header','', TRUE);
			$data['footer'] =$this->load->view('forum/footer','', TRUE);
			$data['view'] =$this->load->view('forum/view_forum',$thread, TRUE);
			$data['side'] =$this->load->view('side_menu',$side, TRUE);
			$this->load->view('view',$data);
	}

	public function set_pagination()
    {
        $this->page_config['first_link']         = '&lsaquo; First';
        $this->page_config['first_tag_open']     = '<li>';
        $this->page_config['first_tag_close']    = '</li>';
        $this->page_config['last_link']          = 'Last &raquo;';
        $this->page_config['last_tag_open']      = '<li>';
        $this->page_config['last_tag_close']     = '</li>';
        $this->page_config['next_link']          = 'Next &rsaquo;';
        $this->page_config['next_tag_open']      = '<li>';
        $this->page_config['next_tag_close']     = '</li>';
        $this->page_config['prev_link']          = '&lsaquo; Prev';
        $this->page_config['prev_tag_open']      = '<li>';
        $this->page_config['prev_tag_close']     = '</li>';
        $this->page_config['cur_tag_open']       = '<li class="active"><a href="javascript://">';
        $this->page_config['cur_tag_close']      = '</a></li>';
        $this->page_config['num_tag_open']       = '<li>';
        $this->page_config['num_tag_close']      = '</li>';
    }
        function category($slug, $start=0)
    {   
        $category = $this->db->get_where(TBL_CAT, array('slug'=>$slug))->row();
        $this->data['cat'] = $this->mcats->category_get_all_parent($category->id,0);
        $this->data['thread'] = $category;

        $cat_id = array();
        $child_cat = $this->mcats->category_get_all($category->id);
        $cat_id[0] = $category->id;
        foreach ($child_cat as $cat) {
                $cat_id[] = $cat['id'];
            }   

        $this->page_config['base_url'] = site_url('fazharamirorum/category/'.$slug);
        $this->page_config['uri_segment'] = 4;
        $this->page_config['total_rows'] = $this->mforum->get_total_by_category($cat_id);
        $this->page_config['per_page']    = 10;
        $this->set_pagination();
        $this->pagination->initialize($this->page_config);
        $thread['page']    = $this->pagination->create_links();
        $menu['menunav']=$this->mposts->menunav();

        
        $thread['threads'] = $this->mforum->get_by_category($start, $this->page_config['per_page'],$cat_id);
        $data['menunav_view']=$this->load->view('forum/menunav',$menu,TRUE);
        $data['header'] =$this->load->view('forum/header','', TRUE);
        $data['footer'] =$this->load->view('forum/footer','', TRUE);
        $data['view'] =$this->load->view('view_forum',$thread, TRUE);
        $side['categories']    = $this->mcats->category_get_all();
        $data['side'] =$this->load->view('side_menu',$side, TRUE);
        $this->load->view('view',$data);
    }

    public function talk($slug, $start = 0)
    {
        $idku = $this->session->userdata('group_id');
        $cek = $this->check_role->cek_role($idku);

        if ($this->input->post('btn-post')) {
            if (!$this->session->userdata('user_id')) {
                redirect('auth/login');
            } else if ($cek['thread_create'] == 0) {
                redirect('forum');
            }

            $this->mthread->reply();

        if ($this->mthread->error_count != 0) {
        
            $this->data['error'] = $this->mthread->error;
        
        } else {    
        
            $this->session->set_userdata('tmp_success',1);
        
            redirect('forum/talk/'.$slug.'/'.$start);
        }

        }

        $tmp_success_new = $this->session->userdata('tmp_success');

        if ($tmp_success_new != null) {
            // new thread created           
            $this->session->set_userdata('tmp_success_new');
            
            $this->data['tmp_success_new'] = 1;
        }

        $tmp_success_new = $this->session->set_userdata('tmp_success');
        if ($tmp_success_new != null) {
            $this->session->unset_userdata('tmp_success');
            $this->data['tmp_success']=1;
        }

         $thread = $this->db->get_where(TBL_THREAD, array('slug' => $slug))->row();
        
        // set pagination
        $this->load->library('pagination');
        $this->page_config['base_url']    = site_url('forum/talk/'.$slug);
        $this->page_config['uri_segment'] = 4;
        $this->page_config['total_rows']  = $this->db->get_where(TBL_POST, array('thread_id' => $thread->id))->num_rows();
        $this->page_config['per_page']    = 10;
        
        $this->set_pagination();
        
        $this->pagination->initialize($this->page_config);

        $posts  = $this->mthread->get_posts($thread->id, $start, $this->page_config['per_page']);
        //$this->thread_model->get_posts_threaded($thread->id, $start, $this->page_config['per_page']);
        $this->data['cat']    = $this->mcats->category_get_all_parent($thread->category_id, 0);
        $menu['menunav']=$this->mposts->menunav();
        $this->data['menunav_view']=$this->load->view('forum/menunav',$menu,TRUE);
        $this->data['categories']    = $this->mcats->category_get_all();
        $this->data['page']   = $this->pagination->create_links();
        $this->data['thread'] = $thread;
        $this->data['posts']  = $posts;
        $side['categories']    = $this->mcats->category_get_all();
        $this->data['side'] =$this->load->view('side_menu',$side, TRUE);
        $this->load->view('forum/header');
        $this->load->view('talk', $this->data);
        $this->load->view('footer');
    }

	
}

/* End of file index.php */
/* Location: ./application/controllers/index.php */
?>