 <div class="widget">
        <ul>
            <h3><b>Categories</b></h3>
            <?php foreach($categories as $cat): ?>
            <li><a href="<?php echo site_url('forum/category/'.$cat['slug']); ?>"><?php echo $cat['name']; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>