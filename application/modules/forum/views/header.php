	<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Potret Bekasi</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/media-queries.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/flexslider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/gallery.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/slide.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.3.2.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/slide.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ddaccordion.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ddaccordion-init.js" ></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/menu-responsive.js" ></script>

	<script src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>

	<script type="text/javascript" charset="utf-8">
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			controlsContainer: ".flex-container"
		});
	});
	</script>

</head>
<body>
	
	<div id="pagewrap">
		<header id="header">
<hgroup class="logo-header">
	<?php if ($this->session->userdata('user_id') == TRUE) { ?>
		<div id="top-login">
			<h4><a href="<?php base_url();?>redaksi">Dashboard</a></h4>
		</div>
		
		<div id="top-pass">
			<h4><?php echo $this->session->userdata('username'); ?> |</h4>
		</div>
		
	</hgroup>
	<?php } else { ?>

				<div id="top-login">
					<h4><a href="<?php base_url();?>auth/register">Daftar</a></h4>
				</div>
				<div id="top-pass">
					<h4><a href="<?php base_url();?>auth/login">Login |</a></h4>
				</div>
			</hgroup>

			<?php } ?>
	<nav class="clearfix">
		<ul class="clearfix">
			<?php 
			foreach ($menunav->result() as $row) {
				echo "<li><a href=".$this->config->item('base_url').$row->link.">".$row->nama_menu."</a></li>";
			}
			?>
		</ul>
		<a href="#" id="pull">Menu</a>
		</nav>

	<form id="searchform">
		<input type="search" id='s' placeholder="search">
	</form>
</header>
