    <div class="judul">     
            <h2>Thread Forum</h2>
            <article class="post clearfix">
<?php
        
        function time_ago($date) {
        if(empty($date)) {
            return "No date provided";
        }
        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60","60","24","7","4.35","12","10");
        $now = time();
        $unix_date = strtotime($date);

        // check validity of date

        if(empty($unix_date)) {
            return "No date";
        }

        // is it future date or past date
        if($now > $unix_date) {
            $difference = $now - $unix_date;
            $tense = "ago";
        } else {
            $difference = $unix_date - $now;
            $tense = "from now";
        }
        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }
        $difference = round($difference);
        if($difference != 1) {
            $periods[$j].= "s";
        }

        return "$difference $periods[$j] {$tense}";
    }
    ?>
 <style>table td, table th {padding:10px 7px !important;} .cat {font-weight:bold;font-size: 10px;color: #333;font-style: italic;}</style>
    <table class="table table-striped table-condensed">

        <thead>
            <tr>
                <th width="85%">All Threads</th>
                <th width="15%">Last Updates</th>
            </tr>

        </thead>

        <tbody>
            <?php foreach($threads as $thread): ?>
            <tr>
            <td style="font-size:12px;">
                <a style="font-family: verdana;" href="<?php echo site_url('forum/talk/'.$thread->slug); ?>"><?php echo $thread->title; ?></a>
                <span style="display: block">
                    <a href="<?php echo site_url('forum/category/'.$thread->category_slug); ?>" class="cat">Category: <?php echo $thread->category_name; ?></a>
                </span>
            </td>
            <td style="font-size:12px;color:#999;vertical-align: middle;">
                <!-- <?php echo date("m/d/y g:i A", strtotime($thread->date_add)); ?> -->
                <?php echo time_ago($thread->date_add); ?>
            </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="pagination" style="text-align:center;">
        <ul><?php echo $page; ?></ul>
    </div>
                </article>
        </div>