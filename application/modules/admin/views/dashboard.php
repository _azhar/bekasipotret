<!DOCTYPE html>
<html lang="en">
<head>
	<!--
		Charisma v1.0.0

		Copyright 2012 Muhammad Usman
		Licensed under the Apache License v2.0
		http://www.apache.org/licenses/LICENSE-2.0

		http://usman.it
		http://twitter.com/halalit_usman
	-->
	<meta charset="utf-8">
	<title>Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">

	<!-- The styles -->
	<link id="bs-css" href="css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/bootstrap-spacelab.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/bootstrap-cerulean.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/bootstrap-responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/charisma-app.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/fullcalendar.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/fullcalendar.print.css" media='print'>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/chosen.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/uniform.default.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/colorbox.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery.cleditor.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery.noty.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/noty_theme_default.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/elfinder.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/elfinder.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/jquery.iphone.toggle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/opa-icons.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/css/uploadify.css">
	
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/admin/images/favicon.ico">
		
</head>

<body>
		<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.html"> <img alt="Charisma Logo" src="<?php echo base_url(); ?>assets/admin/images/logo20.png" /> <span>Bekasi</span></a>
				
				<!-- theme selector starts -->
				<div class="btn-group pull-right theme-container" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-tint"></i><span class="hidden-phone"> Change Theme / Skin</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" id="themes">
						<li><a data-value="classic" href="#"><i class="icon-blank"></i> Classic</a></li>
						<li><a data-value="cerulean" href="#"><i class="icon-blank"></i> Cerulean</a></li>
						<li><a data-value="cyborg" href="#"><i class="icon-blank"></i> Cyborg</a></li>
						<li><a data-value="redy" href="#"><i class="icon-blank"></i> Redy</a></li>
						<li><a data-value="journal" href="#"><i class="icon-blank"></i> Journal</a></li>
						<li><a data-value="simplex" href="#"><i class="icon-blank"></i> Simplex</a></li>
						<li><a data-value="slate" href="#"><i class="icon-blank"></i> Slate</a></li>
						<li><a data-value="spacelab" href="#"><i class="icon-blank"></i> Spacelab</a></li>
						<li><a data-value="united" href="#"><i class="icon-blank"></i> United</a></li>
					</ul>
				</div>
				<!-- theme selector ends -->
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"> admin</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#">Profile</a></li>
						<li class="divider"></li>
						<li><a href="login.html">Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
				<div class="top-nav nav-collapse">
					<ul class="nav">
						<li><a href="#">Visit Site</a></li>
						<li>
							<form class="navbar-search pull-left">
								<input placeholder="Search" class="search-query span2" name="query" type="text">
							</form>
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Main</li>
						<li><a class="ajax-link" href="<?php echo base_url();?>admin/index"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>admin/user_manager"><i class="icon-eye-open"></i><span class="hidden-tablet"> User Manager</span></a></li>
						<li><a class="ajax-link" href="form.html"><i class="icon-edit"></i><span class="hidden-tablet"> Post</span></a></li>
						<li><a class="ajax-link" href="#"><i class="icon-list-alt"></i><span class="hidden-tablet"> Back up data</span></a></li>
						<li><a class="ajax-link" href="#"><i class="icon-picture"></i><span class="hidden-tablet"> Gallery</span></a></li>
					</ul>
					<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> </label>
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Dashboard</a>
					</li>
				</ul>
			</div>
			<div class="sortable row-fluid">
				<a data-rel="tooltip" title="6 new members." class="well span3 top-block" href="#">
					<span class="icon32 icon-red icon-user"></span>
					<div>Total Members</div>
					<div>507</div>
					<span class="notification">6</span>
				</a>

				<a data-rel="tooltip" title="4 new posts." class="well span3 top-block" href="#">
					<span class="icon32 icon-color icon-star-on"></span>
					<div>Recents Posts</div>
					<div>228</div>
					<span class="notification green">4</span>
				</a>

				<a data-rel="tooltip" title="12 new messages." class="well span3 top-block" href="#">
					<span class="icon32 icon-color icon-envelope-closed"></span>
					<div>Messages</div>
					<div>25</div>
					<span class="notification red">12</span>
				</a>
			</div>
			
			<div class="row-fluid">
				<div class="box span12">
					<div class="box-header well">
						<h2><i class="icon-info-sign"></i> Introduction</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<h1>Dashboard Portal Bekasi</h1>
						<p>Monotonectally innovate progressive supply chains before team driven vortals. Completely conceptualize open-source catalysts for change after client-centric sources. Assertively incubate diverse information for one-to-one e-markets. Assertively e-enable orthogonal relationships rather than virtual ideas. Credibly provide access to open-source sources whereas inexpensive leadership.</p>
						<p><b>All pages in the menu are functional, take a look at all, please share this with your followers.</b></p>
						
					
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
					
			<div class="row-fluid sortable">
				<div class="box span4">
					<div class="box-header well">
						<h2><i class="icon-th"></i> Tabs</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<ul class="nav nav-tabs" id="myTab">
							<li class="active"><a href="#info">Info</a></li>
							<li><a href="#custom">Custom</a></li>
							<li><a href="#messages">Messages</a></li>
						</ul>
						 
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane active" id="info">
								<h3>Charisma <small>a fully featued template</small></h3>
								<p>Its a fully featured, responsive template for your admin panel. Its optimized for tablet and mobile phones. Scan the QR code below to view it in your mobile device.</p> <img alt="QR Code" class="charisma_qr center" src="img/qrcode136.png" />
							</div>
							<div class="tab-pane" id="custom">
								<h3>Custom <small>small text</small></h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at. Nulla tellus elit, varius non commodo eget, mattis vel eros. In sed ornare nulla. Donec consectetur, velit a pharetra ultricies, diam lorem lacinia risus, ac commodo orci erat eu massa. Sed sit amet nulla ipsum. Donec felis mauris, vulputate sed tempor at, aliquam a ligula. Pellentesque non pulvinar nisi.</p>
							</div>
							<div class="tab-pane" id="messages">
								<h3>Messages <small>small text</small></h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at. Nulla tellus elit, varius non commodo eget, mattis vel eros. In sed ornare nulla. Donec consectetur, velit a pharetra ultricies, diam lorem lacinia risus, ac commodo orci erat eu massa. Sed sit amet nulla ipsum. Donec felis mauris, vulputate sed tempor at, aliquam a ligula. Pellentesque non pulvinar nisi.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor.</p>
							</div>
						</div>
					</div>
				</div><!--/span-->
						
				<div class="box span4">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Member Activity</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<div class="box-content">
							<ul class="dashboard-list">
								<li>
									<a href="#">
										<img class="dashboard-avatar" alt="Usman" src="http://www.gravatar.com/avatar/f0ea51fa1e4fae92608d8affee12f67b.png?s=50"></a>
										<strong>Name:</strong> <a href="#">Usman
									</a><br>
									<strong>Since:</strong> 17/05/2012<br>
									<strong>Status:</strong> <span class="label label-success">Approved</span>                                  
								</li>
								<li>
									<a href="#">
										<img class="dashboard-avatar" alt="Sheikh Heera" src="http://www.gravatar.com/avatar/3232415a0380253cfffe19163d04acab.png?s=50"></a>
										<strong>Name:</strong> <a href="#">Sheikh Heera
									</a><br>
									<strong>Since:</strong> 17/05/2012<br>
									<strong>Status:</strong> <span class="label label-warning">Pending</span>                                 
								</li>
								<li>
									<a href="#">
										<img class="dashboard-avatar" alt="Abdullah" src="http://www.gravatar.com/avatar/46056f772bde7c536e2086004e300a04.png?s=50"></a>
										<strong>Name:</strong> <a href="#">Abdullah
									</a><br>
									<strong>Since:</strong> 25/05/2012<br>
									<strong>Status:</strong> <span class="label label-important">Banned</span>                                  
								</li>
								<li>
									<a href="#">
										<img class="dashboard-avatar" alt="Saruar Ahmed" src="http://www.gravatar.com/avatar/564e1bb274c074dc4f6823af229d9dbb.png?s=50"></a>
										<strong>Name:</strong> <a href="#">Saruar Ahmed
									</a><br>
									<strong>Since:</strong> 17/05/2012<br>
									<strong>Status:</strong> <span class="label label-info">Updates</span>                                  
								</li>
							</ul>
						</div>
					</div>
				</div><!--/span-->
						
				
				<div class="box span4">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-list"></i> Weekly Stat</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<ul class="dashboard-list">
							<li>
								<a href="#">
									<i class="icon-arrow-up"></i>                               
									<span class="green">92</span>
									New Comments                                    
								</a>
							</li>
						  <li>
							<a href="#">
							  <i class="icon-arrow-down"></i>
							  <span class="red">15</span>
							  New Registrations
							</a>
						  </li>
						  <li>
							<a href="#">
							  <i class="icon-minus"></i>
							  <span class="blue">36</span>
							  New Articles                                    
							</a>
						  </li>
						  <li>
							<a href="#">
							  <i class="icon-comment"></i>
							  <span class="yellow">45</span>
							  User reviews                                    
							</a>
						  </li>
						  <li>
							<a href="#">
							  <i class="icon-arrow-up"></i>                               
							  <span class="green">112</span>
							  New Comments                                    
							</a>
						  </li>
						  <li>
							<a href="#">
							  <i class="icon-arrow-down"></i>
							  <span class="red">31</span>
							  New Registrations
							</a>
						  </li>
						  <li>
							<a href="#">
							  <i class="icon-minus"></i>
							  <span class="blue">93</span>
							  New Articles                                    
							</a>
						  </li>
						  <li>
							<a href="#">
							  <i class="icon-comment"></i>
							  <span class="yellow">254</span>
							  User reviews                                    
							</a>
						  </li>
						</ul>
					</div>
				</div><!--/span-->
			</div><!--/row-->

					
			</div><!--/row-->
				  

		  
       
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<footer>
			<p class="pull-left">&copy; <a href="http://usman.it" target="_blank">Muhammad Usman</a> 2012</p>
			<p class="pull-right">Powered by: <a href="http://usman.it/free-responsive-admin-template">Charisma</a></p>
		</footer>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/fullcalendar.min.js"></script>
	<!-- data table plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.dataTables.min.js"></script>

	<!-- chart libraries start -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/excanvas.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.pie.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.stack.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.flot.resize.min.js"></script>

	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.chosen.min.js"></script>

	<!-- checkbox, radio, and file input styler -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.uniform.min.js"></script>

	<!-- plugin for gallery image view -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.colorbox.min.js"></script>

	<!-- rich text editor library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.cleditor.min.js"></script>

	<!-- notification plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.noty.js"></script>

	<!-- file manager library -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.elfinder.min.js"></script>

	<!-- star rating plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.raty.min.js"></script>

	<!-- for iOS style toggle switch -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.iphone.toggle.js"></script>

	<!-- autogrowing textarea plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.autogrow-textarea.js"></script>

	<!-- multiple file upload plugin -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.uploadify-3.1.min.js"></script>

	<!-- history.js for cross-browser state change on ajax -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.history.js"></script>

	<!-- application script for Charisma demo -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/charisma.js"></script>
		
</body>
</html>