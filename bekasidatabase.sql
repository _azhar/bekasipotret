-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 25, 2013 at 10:05 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bekasidatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  `publish` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `name`, `slug`, `date_added`, `date_edit`, `publish`) VALUES
(11, 0, 'Web Programming', 'web-programming', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(12, 11, 'PHP', 'php', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(13, 0, 'Web Design', 'web-design', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(14, 13, 'CSS', 'css', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(15, 12, 'Beginner & Installation', 'php-beginner-installation', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(16, 12, 'Session, Cookie, Security', 'php-session-cookie-security', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(17, 12, 'File & Image Manipulation', 'php-file-image-manipulation', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(18, 14, 'Responsive Layout', 'css-responsive-layout', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(19, 14, 'Beginner', 'css-beginner', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(20, 14, 'Effect, Animation, Gradient', 'css-effect-animation-gradient', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(21, 11, 'Javascript', 'javascript', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(22, 21, 'Jquery', 'jquery', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(23, 0, 'Project Managementasos', 'project-management', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(24, 14, 'qwdsfcasf', 'xcvzxvcxz', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_news`
--

CREATE TABLE IF NOT EXISTS `category_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `shortdesc` text NOT NULL,
  `longdesc` text NOT NULL,
  `status` enum('Y','N') NOT NULL,
  `sortorder` int(7) NOT NULL,
  `parentid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `category_news`
--

INSERT INTO `category_news` (`id`, `name`, `shortdesc`, `longdesc`, `status`, `sortorder`, `parentid`) VALUES
(1, 'Berita Olahraga', 'Berita tentang olahraga', 'Semua berita tentang olahraga', 'Y', 3, 2),
(2, 'Berita Politik', 'Politik money', 'Politik serba unggul', 'Y', 0, 0),
(3, 'Kebudayaan', 'Tentang kebudayaan yang ada di Indonesia', 'Kebudayaan adalah jos', 'Y', 3, 2),
(4, 'Wisata', 'Tentang wisata', 'Tentang serba serbi wisata', 'Y', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `category_post`
--

CREATE TABLE IF NOT EXISTS `category_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_id` int(11) NOT NULL,
  `reply_to_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `post` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `category_post`
--

INSERT INTO `category_post` (`id`, `thread_id`, `reply_to_id`, `author_id`, `post`, `date_add`, `date_edit`) VALUES
(4, 3, 0, 5, 'I have a datetime column in mysql which I need to convert to mm/dd/yy H:M (AM/PM) using PHP.', '2012-08-06 06:18:59', '0000-00-00 00:00:00'),
(5, 4, 0, 5, 'Is there a simple way to convert one date format into another date format in PHP?', '2012-08-06 06:19:38', '0000-00-00 00:00:00'),
(62, 10, 0, 5, 'My first cry for help here. Not sure if my title is properly explicit but it''s the only one I can come up with right now. I''ve been at it for 2 days now and I have read so many different things that I ...', '2012-08-13 06:38:42', '0000-00-00 00:00:00'),
(63, 11, 0, 5, 'How can I remove ''index.php'' from urls, if I have some controllers in the controllers folder and one in subfolder? For example my frontend url looks like this : domain.com/site/contact.html I would like my backend url look like this: domain.com/system/settings/profile.html, where system is not a controller, only a subfolder in the controllers folder. When I type domain.com/index.php/system/settings/profile.html, everything works fine, it just does not look right. Here''s what''s in my routes.php file:', '2012-08-13 07:16:03', '0000-00-00 00:00:00'),
(64, 7, 0, 5, 'fghghgthrth', '2012-08-13 07:34:58', '0000-00-00 00:00:00'),
(90, 9, 61, 5, 'which can be edited manually. I want to add/remove php values from \r\nanother div having a set of php array values (from a query). Each value \r\nhave an [Add', '2012-08-15 07:30:44', '0000-00-00 00:00:00'),
(97, 15, 0, 33, 'Java Script Thats All', '2013-01-08 11:19:29', '0000-00-00 00:00:00'),
(108, 15, 0, 5, 'Saya tidak bisa melakukan login dengan java script, any help me ?', '2013-01-30 19:12:09', '0000-00-00 00:00:00'),
(113, 12, 0, 7, 'nbnxnbxbxhx', '2013-02-04 17:05:25', '0000-00-00 00:00:00'),
(115, 15, 0, 7, 'DDDDDD', '2013-02-11 17:43:26', '0000-00-00 00:00:00'),
(116, 15, 115, 7, '<div style="font-size:11px; background: #e3e3e3;padding:5px;">posted by <b>@donny</b><p><i>DDDDDD</i></p></div><br>ddddddddddddd', '2013-02-11 18:19:36', '0000-00-00 00:00:00'),
(117, 16, 0, 7, 'djjjjjjjjjdndndndndnd', '2013-02-11 21:11:13', '0000-00-00 00:00:00'),
(118, 16, 117, 7, '<div style="font-size:11px; background: #e3e3e3;padding:5px;">posted by <b>@donny</b><p><i>djjjjjjjjjdndndndndnd</i></p></div><br>saadascascas', '2013-02-18 10:08:23', '0000-00-00 00:00:00'),
(119, 16, 0, 7, 'dswsdfwd', '2013-02-18 13:35:15', '0000-00-00 00:00:00'),
(120, 16, 0, 7, 'dsqqdqsdxdsx', '2013-02-18 13:35:18', '0000-00-00 00:00:00'),
(121, 16, 0, 7, 'dsaaxasxaxaxasx', '2013-02-18 13:35:24', '0000-00-00 00:00:00'),
(122, 16, 0, 7, 'ssssssssssssssss', '2013-02-18 13:39:08', '0000-00-00 00:00:00'),
(123, 16, 121, 7, '<div style="font-size:11px; background: #e3e3e3;padding:5px;">posted by <b>@admin</b><p><i>dsaaxasxaxaxasx</i></p></div><br>xxxxxxxxxxxxxx', '2013-02-18 13:44:01', '0000-00-00 00:00:00'),
(124, 16, 0, 5, 'saya', '2013-02-18 20:42:37', '0000-00-00 00:00:00'),
(125, 16, 0, 5, 'sss', '2013-02-18 20:55:32', '0000-00-00 00:00:00'),
(127, 16, 0, 5, '<font face="Verdana" size="2"><b>Holisticly implement empowered results vis-a-vis integrated e-tailers. Globally harness e-business supply chains and magnetic deliverables. Competently supply timely deliverables via scalable applications. Authoritatively fashion robust networks and stand-alone networks. Compellingly drive ubiquitous technologies before seamless.</b></font>', '2013-02-19 10:16:31', '0000-00-00 00:00:00'),
(128, 18, 0, 5, 'kcwdjkcdccccccccccccccccccccc', '2013-02-19 12:10:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `category_thread`
--

CREATE TABLE IF NOT EXISTS `category_thread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  `date_last_post` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `category_thread`
--

INSERT INTO `category_thread` (`id`, `category_id`, `title`, `slug`, `date_add`, `date_edit`, `date_last_post`) VALUES
(3, 12, 'Format mysql datetime with php', 'format-mysql-datetime-with-php', '2012-08-06 06:18:59', '0000-00-00 00:00:00', '2012-08-06 06:18:59'),
(4, 12, 'Convert one date format into another in PHP', 'convert-one-date-format-into-another-in-php', '2012-08-06 06:19:38', '0000-00-00 00:00:00', '2012-08-06 06:19:38'),
(6, 18, 'Fluid images in table - Width: CSS vs HTML', 'fluid-images-in-table---width:-css-vs-html', '2012-08-07 13:11:09', '0000-00-00 00:00:00', '2012-08-07 13:11:09'),
(7, 12, 'How to add RDoc documentation for a method defined using class_eval?', 'how-to-add-rdoc-documentation-for-a-method-defined-using-classeval', '2012-08-13 06:33:48', '0000-00-00 00:00:00', '2012-08-13 06:33:48'),
(9, 11, 'transfer (add/remove) php array values between 2 divs', 'transfer-addremove-php-array-values-between-2-divs', '2012-08-13 06:38:03', '0000-00-00 00:00:00', '2012-08-13 06:38:03'),
(10, 11, 'Creating an associative array from PHP through AJAX in JQUERY', 'creating-an-associative-array-from-php-through-ajax-in-jquery', '2012-08-13 06:38:42', '0000-00-00 00:00:00', '2012-08-13 06:38:42'),
(11, 12, 'Codeigniter - controllers in subfolder, remove index.php from url', 'codeigniter--controllers-in-subfolder-remove-indexphp-from-url', '2012-08-13 07:16:03', '0000-00-00 00:00:00', '2012-08-13 07:16:03'),
(12, 13, '10-custom-controls.html', '10customcontrolshtml', '2012-08-14 06:15:57', '0000-00-00 00:00:00', '2012-08-14 06:15:57'),
(15, 23, 'FORM TABLE HTML', 'dadAD', '2013-02-11 17:43:26', '0000-00-00 00:00:00', '2013-02-11 17:43:26'),
(16, 13, 'sayasshshsh', 'shshshsh', '2013-02-11 21:11:13', '0000-00-00 00:00:00', '2013-02-11 21:11:13'),
(18, 23, 'thread', 'jwd', '2013-02-19 12:10:22', '0000-00-00 00:00:00', '2013-02-19 12:10:22');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `group_roles`
--

CREATE TABLE IF NOT EXISTS `group_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) NOT NULL,
  `admin_area` int(1) NOT NULL DEFAULT '0',
  `thread_create` int(1) NOT NULL DEFAULT '0',
  `thread_edit` int(1) NOT NULL DEFAULT '0',
  `thread_delete` int(1) NOT NULL DEFAULT '0',
  `post_create` int(1) NOT NULL DEFAULT '0',
  `post_edit` int(1) NOT NULL DEFAULT '0',
  `post_delete` int(1) NOT NULL DEFAULT '0',
  `role_create` int(1) NOT NULL DEFAULT '0',
  `role_edit` int(1) NOT NULL DEFAULT '0',
  `role_delete` int(1) NOT NULL DEFAULT '0',
  `category_create` int(11) NOT NULL DEFAULT '0',
  `category_delete` int(1) NOT NULL DEFAULT '0',
  `category_edit` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `group_roles`
--

INSERT INTO `group_roles` (`id`, `role`, `admin_area`, `thread_create`, `thread_edit`, `thread_delete`, `post_create`, `post_edit`, `post_delete`, `role_create`, `role_edit`, `role_delete`, `category_create`, `category_delete`, `category_edit`) VALUES
(1, 'administrators', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 'wartawan', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 'membersss', 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0),
(6, 'member bengal', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_berita` int(8) NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `id_berita`, `image`) VALUES
(11, 61, 'One_Piece.jpg'),
(12, 61, '00_Gundam_7S_wallpaper2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `kategori`) VALUES
(1, 'Politik'),
(2, 'Olahraga'),
(3, 'Otomotif'),
(4, 'Kesehatan'),
(5, 'Kuliner'),
(6, 'Teknologi'),
(7, 'Lifestyle'),
(8, 'Hukum'),
(9, 'Peristiwa');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menunav`
--

CREATE TABLE IF NOT EXISTS `menunav` (
  `id_main` int(11) NOT NULL,
  `nama_menu` varchar(35) NOT NULL,
  `link` varchar(45) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  `id_menu_static` varchar(35) NOT NULL,
  PRIMARY KEY (`id_main`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menunav`
--

INSERT INTO `menunav` (`id_main`, `nama_menu`, `link`, `aktif`, `id_menu_static`) VALUES
(1, 'Home', 'main', 'Y', '0'),
(2, 'News', 'main/news', 'Y', '0'),
(3, 'Forum', 'forum', 'Y', '0'),
(4, 'Gallery', 'main/gallery', 'Y', '');

-- --------------------------------------------------------

--
-- Table structure for table `menu_head`
--

CREATE TABLE IF NOT EXISTS `menu_head` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nama` varchar(55) NOT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `menu_head`
--

INSERT INTO `menu_head` (`id`, `nama`, `aktif`) VALUES
(1, 'Main', 'Y'),
(2, 'Berita', 'Y'),
(3, 'Data', 'Y'),
(4, 'Account User', 'Y'),
(5, 'Forum', 'Y'),
(6, 'Management user', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `menu_left`
--

CREATE TABLE IF NOT EXISTS `menu_left` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nama` varchar(55) NOT NULL,
  `link` varchar(55) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  `level` enum('admin','member','warta') NOT NULL,
  `kode_id` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `menu_left`
--

INSERT INTO `menu_left` (`id`, `nama`, `link`, `aktif`, `level`, `kode_id`) VALUES
(1, 'Home', 'redaksi_main', 'Y', '', 1),
(2, 'Iklan', '#', 'Y', '', 1),
(3, 'Back Up', '#', 'Y', '', 1),
(4, 'Pencarian', '#', 'Y', '', 1),
(5, 'Buat Baru', 'buat_baru', 'Y', '', 2),
(6, 'Status Berita', 'status_berita', 'Y', '', 2),
(7, 'Lihat Berita', 'lihat_berita', 'Y', '', 2),
(8, 'Berita Saya', 'berita_saya', 'Y', '', 2),
(9, 'Kategori', 'kategori', 'Y', '', 3),
(10, 'Pewarta', 'pewarta', 'Y', '', 3),
(11, 'Ganti Password', '#', 'Y', '', 4),
(12, 'Buat Account', '#', 'Y', '', 4),
(13, 'News', 'news', 'Y', '', 5),
(14, 'Category', 'category', 'Y', '', 5),
(15, 'Posts', '#', 'Y', '', 5),
(16, 'My Posts', '#', 'Y', '', 5),
(17, 'View Role', 'view_role', 'Y', '', 6),
(18, 'Create Roles', 'role_create', 'Y', '', 6),
(19, 'View User', 'user_view', 'Y', '', 6);

-- --------------------------------------------------------

--
-- Table structure for table `pewarta`
--

CREATE TABLE IF NOT EXISTS `pewarta` (
  `id_pewarta` int(11) NOT NULL,
  `nama_pewarta` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pewarta`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pewarta`
--

INSERT INTO `pewarta` (`id_pewarta`, `nama_pewarta`, `email`) VALUES
(3, 'Crash Bandicoot', 'water7wall@gmail.com'),
(4, 'Fahmy Ardiman', 'fahmy_ard@hotmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('baru','disetujui','ditolak') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'baru',
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `pubdate` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `teraktual` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `inspiratif` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `bermanfaat` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `menarik` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=62 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `tags`, `status`, `body`, `category_id`, `pubdate`, `user_id`, `teraktual`, `link`, `inspiratif`, `bermanfaat`, `menarik`) VALUES
(30, 'Dasar Hukum', 'dasar hukum', 'baru', '<p>Progressively e-enable premium web services and quality strategic theme areas. Proactively communicate market positioning portals without cost effective functionalities. Completely incubate turnkey web-readiness vis-a-vis magnetic web services. Proactively harness resource sucking core competencies after client-centric infrastructures. Globally scale backend value before adaptive innovation.</p>\n<div>&nbsp;</div>', 8, '2012-12-27 07:53:28', 4, 'N', '#', 'Y', 'Y', 'Y'),
(31, 'Keputusan Presiden', 'hukum nasional', 'baru', '<p>Collaboratively iterate standards compliant materials with diverse ideas. Appropriately pursue best-of-breed mindshare and corporate methods of empowerment. Credibly provide access to scalable relationships vis-a-vis state of the art growth strategies. Compellingly plagiarize intermandated mindshare whereas interactive e-commerce. Compellingly administrate dynamic testing procedures via collaborative technologies.</p>\n<div>&nbsp;</div>', 8, '2012-12-27 08:01:19', 4, 'Y', '#', 'Y', 'Y', 'Y'),
(33, 'Maling digebukin', 'maling', 'disetujui', '<p>Phosfluorescently disintermediate seamless initiatives before enterprise action items. Enthusiastically myocardinate ubiquitous interfaces without collaborative leadership. Synergistically coordinate client-centered schemas vis-a-vis customized results. Interactively coordinate effective quality vectors before go forward ROI. Phosfluorescently foster enterprise niche markets without emerging imperatives.</p>\n<p>&nbsp;</p>\n<p>Professionally pontificate resource maximizing value after e-business solutions. Credibly re-engineer cross-media deliverables vis-a-vis collaborative platforms. Proactively enhance collaborative bandwidth and tactical bandwidth. Phosfluorescently mesh low-risk high-yield supply chains for extensible potentialities. Rapidiously recaptiualize extensible process improvements through out-of-the-box models.</p>\n<p>&nbsp;</p>\n<p>Synergistically revolutionize inexpensive products and magnetic expertise. Globally grow client-focused manufactured products and 2.0 testing procedures. Assertively generate functionalized results through diverse internal or "organic" sources. Objectively facilitate viral best practices without turnkey networks. Dynamically repurpose top-line ROI without mission-critical leadership.</p>\n<p>&nbsp;</p>\n<p>Objectively transition intuitive niches before intuitive technologies. Completely administrate pandemic metrics through state of the art customer service. Continually exploit tactical manufactured products after wireless potentialities. Energistically synergize enabled niches through standards compliant outsourcing. Efficiently maximize enterprise bandwidth via collaborative markets.</p>\n<p>&nbsp;</p>\n<p>Globally exploit technically sound interfaces without orthogonal partnerships. Distinctively facilitate unique web-readiness without best-of-breed materials. Energistically transform client-based intellectual capital via backend markets. Authoritatively initiate performance based solutions rather than functional resources. Dynamically recaptiualize high-payoff web-readiness and cooperative niches.</p>\n<p>&nbsp;</p>\n<p>Efficiently architect granular process improvements via low-risk high-yield best practices. Energistically reintermediate front-end web services for enterprise leadership skills. Seamlessly recaptiualize maintainable solutions with viral materials. Objectively create robust outsourcing vis-a-vis backward-compatible innovation. Monotonectally parallel task optimal web-readiness vis-a-vis orthogonal vortals.</p>\n<p>&nbsp;</p>\n<p>Quickly fashion world-class potentialities rather than granular testing procedures. Authoritatively fabricate high standards in networks via enterprise-wide paradigms. Globally redefine high standards in systems via client-based collaboration and.</p>', 9, '2013-01-03 22:30:33', 4, 'N', '#', 'N', 'N', 'Y'),
(34, 'Maling kejebur', 'maling', 'disetujui', '<p>Interactively visualize sticky mindshare whereas cross-unit infomediaries. Holisticly exploit scalable ideas via strategic potentialities. Seamlessly simplify open-source best practices with distinctive benefits. Appropriately deliver viral relationships via pandemic benefits. Dynamically benchmark vertical value after corporate interfaces.</p>\n<p>&nbsp;</p>\n<p>Competently supply cross-platform niches whereas standardized catalysts for change. Intrinsicly implement user friendly channels through interoperable functionalities. Globally scale go forward information with front-end services. Completely generate seamless content vis-a-vis revolutionary e-markets. Dynamically actualize fully researched ideas rather than process-centric strategic theme areas.</p>\n<p>&nbsp;</p>\n<p>Dynamically reconceptualize leading-edge solutions through collaborative technology. Dynamically formulate empowered "outside the box" thinking before just in time process improvements. Interactively actualize multifunctional initiatives for wireless customer service. Objectively grow adaptive services for market-driven resources. Credibly negotiate global e-services rather than cooperative supply chains.</p>\n<p>&nbsp;</p>\n<p>Assertively visualize adaptive content for synergistic information. Objectively cultivate stand-alone leadership skills whereas intuitive e-commerce. Distinctively reconceptualize global experiences without standards compliant human capital. Enthusiastically actualize 2.0 e-business and equity invested leadership. Energistically redefine 2.0 meta-services via alternative manufactured products.</p>\n<p>&nbsp;</p>\n<p>Globally whiteboard end-to-end users through real-time convergence. Collaboratively develop focused infomediaries via fully researched deliverables. Efficiently incentivize enterprise-wide data after functionalized infomediaries. Phosfluorescently fabricate fully researched e-markets after 24/365 architectures. Synergistically harness alternative intellectual capital vis-a-vis team driven niches.</p>\n<p>&nbsp;</p>\n<p>Assertively streamline end-to-end technology via cross functional e-business. Enthusiastically mesh professional meta-services without inexpensive infomediaries. Uniquely transition process-centric models vis-a-vis open-source technology. Authoritatively fashion interoperable interfaces with collaborative solutions. Holisticly strategize ethical strategic theme areas with dynamic models.</p>\n<p>&nbsp;</p>\n<p>Efficiently create open-source portals without cross-media infrastructures. Enthusiastically grow effective infrastructures through functionalized information. Rapidiously predominate professional alignments without integrated leadership skills. Seamlessly leverage other''s functional initiatives for.</p>', 9, '2013-01-03 22:30:18', 4, 'Y', '#', 'N', 'N', 'Y'),
(35, 'Maling ngantuk', 'maling', 'disetujui', '<p>Interactively visualize sticky mindshare whereas cross-unit infomediaries. Holisticly exploit scalable ideas via strategic potentialities. Seamlessly simplify open-source best practices with distinctive benefits. Appropriately deliver viral relationships via pandemic benefits. Dynamically benchmark vertical value after corporate interfaces.</p>\n<p>&nbsp;</p>\n<p>Competently supply cross-platform niches whereas standardized catalysts for change. Intrinsicly implement user friendly channels through interoperable functionalities. Globally scale go forward information with front-end services. Completely generate seamless content vis-a-vis revolutionary e-markets. Dynamically actualize fully researched ideas rather than process-centric strategic theme areas.</p>\n<p>&nbsp;</p>\n<p>Dynamically reconceptualize leading-edge solutions through collaborative technology. Dynamically formulate empowered "outside the box" thinking before just in time process improvements. Interactively actualize multifunctional initiatives for wireless customer service. Objectively grow adaptive services for market-driven resources. Credibly negotiate global e-services rather than cooperative supply chains.</p>\n<p>&nbsp;</p>\n<p>Assertively visualize adaptive content for synergistic information. Objectively cultivate stand-alone leadership skills whereas intuitive e-commerce. Distinctively reconceptualize global experiences without standards compliant human capital. Enthusiastically actualize 2.0 e-business and equity invested leadership. Energistically redefine 2.0 meta-services via alternative manufactured products.</p>\n<p>&nbsp;</p>\n<p>Globally whiteboard end-to-end users through real-time convergence. Collaboratively develop focused infomediaries via fully researched deliverables. Efficiently incentivize enterprise-wide data after functionalized infomediaries. Phosfluorescently fabricate fully researched e-markets after 24/365 architectures. Synergistically harness alternative intellectual capital vis-a-vis team driven niches.</p>\n<p>&nbsp;</p>\n<p>Assertively streamline end-to-end technology via cross functional e-business. Enthusiastically mesh professional meta-services without inexpensive infomediaries. Uniquely transition process-centric models vis-a-vis open-source technology. Authoritatively fashion interoperable interfaces with collaborative solutions. Holisticly strategize ethical strategic theme areas with dynamic models.</p>\n<p>&nbsp;</p>\n<p>Efficiently create open-source portals without cross-media infrastructures. Enthusiastically grow effective infrastructures through functionalized information. Rapidiously predominate professional alignments without integrated leadership skills. Seamlessly leverage other''s functional initiatives for.</p>', 9, '2013-01-03 22:30:01', 4, 'Y', '#', 'N', 'N', 'Y'),
(36, 'Maling sapi', 'maling', 'ditolak', '<p>Interactively visualize sticky mindshare whereas cross-unit infomediaries. Holisticly exploit scalable ideas via strategic potentialities. Seamlessly simplify open-source best practices with distinctive benefits. Appropriately deliver viral relationships via pandemic benefits. Dynamically benchmark vertical value after corporate interfaces.</p>\n<p>&nbsp;</p>\n<p>Competently supply cross-platform niches whereas standardized catalysts for change. Intrinsicly implement user friendly channels through interoperable functionalities. Globally scale go forward information with front-end services. Completely generate seamless content vis-a-vis revolutionary e-markets. Dynamically actualize fully researched ideas rather than process-centric strategic theme areas.</p>\n<p>&nbsp;</p>\n<p>Dynamically reconceptualize leading-edge solutions through collaborative technology. Dynamically formulate empowered "outside the box" thinking before just in time process improvements. Interactively actualize multifunctional initiatives for wireless customer service. Objectively grow adaptive services for market-driven resources. Credibly negotiate global e-services rather than cooperative supply chains.</p>\n<p>&nbsp;</p>\n<p>Assertively visualize adaptive content for synergistic information. Objectively cultivate stand-alone leadership skills whereas intuitive e-commerce. Distinctively reconceptualize global experiences without standards compliant human capital. Enthusiastically actualize 2.0 e-business and equity invested leadership. Energistically redefine 2.0 meta-services via alternative manufactured products.</p>\n<p>&nbsp;</p>\n<p>Globally whiteboard end-to-end users through real-time convergence. Collaboratively develop focused infomediaries via fully researched deliverables. Efficiently incentivize enterprise-wide data after functionalized infomediaries. Phosfluorescently fabricate fully researched e-markets after 24/365 architectures. Synergistically harness alternative intellectual capital vis-a-vis team driven niches.</p>\n<p>&nbsp;</p>\n<p>Assertively streamline end-to-end technology via cross functional e-business. Enthusiastically mesh professional meta-services without inexpensive infomediaries. Uniquely transition process-centric models vis-a-vis open-source technology. Authoritatively fashion interoperable interfaces with collaborative solutions. Holisticly strategize ethical strategic theme areas with dynamic models.</p>\n<p>&nbsp;</p>\n<p>Efficiently create open-source portals without cross-media infrastructures. Enthusiastically grow effective infrastructures through functionalized information. Rapidiously predominate professional alignments without integrated leadership skills. Seamlessly leverage other''s functional initiatives for.</p>', 9, '2013-01-03 22:31:09', 4, 'N', '#', 'N', 'N', 'Y'),
(37, 'Hukuman Mancung', 'hukum', 'ditolak', '<p  justify;">Objectively deploy top-line partnerships whereas B2B opportunities. Assertively incubate team driven metrics with multimedia based schemas. Professionally maintain high-quality leadership for enabled customer service. Energistically iterate dynamic markets after magnetic information. Phosfluorescently matrix dynamic human capital vis-a-vis prospective manufactured products.</p>\n<div>&nbsp;</div>', 8, '2012-12-27 14:00:12', 3, 'Y', '#', 'N', 'N', 'Y'),
(39, 'Makan Nasi', 'makan', 'baru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae nibh tellus. Maecenas facilisis massa id urna semper ac dictum augue vehicula. Donec eu ligula quis massa auctor blandit. Ut ac vulputate nulla. Sed tempor nibh lobortis odio tempor sed condimentum tortor pharetra? Etiam congue venenatis pharetra. Duis a turpis risus. Etiam in felis nulla, sed porttitor enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec eget egestas nisl. Phasellus pharetra ultricies magna, at commodo turpis volutpat dictum. Mauris semper hendrerit tristique. In rutrum ultrices leo, et laoreet eros pellentesque sed. Duis feugiat erat id orci tristique luctus. Maecenas vehicula lectus ac nulla interdum ornare.\r\n', 3, '2013-02-04 12:29:04', 3, 'N', '#', 'N', 'N', 'Y'),
(40, 'Makan Ayam', 'makan', 'baru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae nibh tellus. Maecenas facilisis massa id urna semper ac dictum augue vehicula. Donec eu ligula quis massa auctor blandit. Ut ac vulputate nulla. Sed tempor nibh lobortis odio tempor sed condimentum tortor pharetra? Etiam congue venenatis pharetra. Duis a turpis risus. Etiam in felis nulla, sed porttitor enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec eget egestas nisl. Phasellus pharetra ultricies magna, at commodo turpis volutpat dictum. Mauris semper hendrerit tristique. In rutrum ultrices leo, et laoreet eros pellentesque sed. Duis feugiat erat id orci tristique luctus. Maecenas vehicula lectus ac nulla interdum ornare.\r\n', 5, '2013-02-04 12:30:42', 3, '', '#', '', '', ''),
(41, 'Makan Sayur', 'makan', 'baru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae nibh tellus. Maecenas facilisis massa id urna semper ac dictum augue vehicula. Donec eu ligula quis massa auctor blandit. Ut ac vulputate nulla. Sed tempor nibh lobortis odio tempor sed condimentum tortor pharetra? Etiam congue venenatis pharetra. Duis a turpis risus. Etiam in felis nulla, sed porttitor enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec eget egestas nisl. Phasellus pharetra ultricies magna, at commodo turpis volutpat dictum. Mauris semper hendrerit tristique. In rutrum ultrices leo, et laoreet eros pellentesque sed. Duis feugiat erat id orci tristique luctus. Maecenas vehicula lectus ac nulla interdum ornare.\r\n', 5, '2013-02-04 12:30:42', 3, '', '#', '', '', ''),
(42, 'Makan Somay', 'makan', 'baru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vitae nibh tellus. Maecenas facilisis massa id urna semper ac dictum augue vehicula. Donec eu ligula quis massa auctor blandit. Ut ac vulputate nulla. Sed tempor nibh lobortis odio tempor sed condimentum tortor pharetra? Etiam congue venenatis pharetra. Duis a turpis risus. Etiam in felis nulla, sed porttitor enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec eget egestas nisl. Phasellus pharetra ultricies magna, at commodo turpis volutpat dictum. Mauris semper hendrerit tristique. In rutrum ultrices leo, et laoreet eros pellentesque sed. Duis feugiat erat id orci tristique luctus. Maecenas vehicula lectus ac nulla interdum ornare.\r\n', 5, '2013-02-04 12:31:19', 3, '', '#', '', '', ''),
(48, 'Jagoan Tertabrak', NULL, 'baru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacinia auctor libero ac rhoncus! Ut ultricies rhoncus risus id varius. Sed id turpis orci. Suspendisse bibendum orci risus, vel mattis justo? Integer eleifend tincidunt nulla sed pulvinar. Cras faucibus nisl et nisi mattis mollis! Integer in lorem magna, ut sagittis tortor. Etiam malesuada mattis dui eu condimentum.<br>', 9, '2013-02-04 17:34:33', 3, 'N', '#', 'N', 'N', 'N'),
(49, 'Motor Miring', NULL, 'baru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacinia auctor libero ac rhoncus! Ut ultricies rhoncus risus id varius. Sed id turpis orci. Suspendisse bibendum orci risus, vel mattis justo? Integer eleifend tincidunt nulla sed pulvinar. Cras faucibus nisl et nisi mattis mollis! Integer in lorem magna, ut sagittis tortor. Etiam malesuada mattis dui eu condimentum.', 3, '2013-02-14 10:35:52', 3, 'N', '#', 'N', 'N', 'N'),
(50, 'Lompat Jurang', NULL, 'baru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id leo vel nunc tincidunt condimentum eu a eros. Pellentesque ullamcorper neque id risus pellentesque eget condimentum tortor pretium. Curabitur vel est at mauris ullamcorper rhoncus. Vestibulum vitae ante lectus, nec auctor eros. Mauris posuere, enim ac congue dapibus, sem nisl dignissim neque, at laoreet magna sapien et risus. Duis vehicula mi nec velit laoreet gravida volutpat risus consequat. Duis mollis dolor nec nisi rutrum mollis. Nullam ligula eros, venenatis dapibus luctus eget, fringilla quis nibh. In sodales felis at lacus fringilla nec aliquam felis dictum. Vestibulum porttitor augue mollis tortor interdum a rhoncus felis congue. Suspendisse vel adipiscing sem. Pellentesque in arcu id augue aliquet cursus id in mi. Aliquam vitae mi urna, eu sodales dui. Praesent id metus arcu, non ullamcorper sem? Sed id ipsum lorem, vitae faucibus sapien!<br><br>', 2, '2013-02-16 10:12:37', 3, 'N', '#', 'N', 'N', 'N'),
(51, 'dwqfdasca', NULL, 'baru', 'asccacascascassssssssssssssssssssssssssssssssssssssssssssssssssssssscccc', 4, '2013-02-18 11:07:14', 7, 'N', '#', 'N', 'N', 'N'),
(61, 'asd', NULL, 'baru', 'gagagagagag', 8, '2013-02-19 19:07:34', 3, 'N', '#', 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `group_id` int(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`, `group_id`) VALUES
(5, 'azhar', '$2a$08$Ng27MvjrilHY5NGIrGonS.dehAA3mlStzjq3tn92f350QyQFNZ63S', 'sid.viciouzz@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2013-02-20 14:57:28', '2013-02-18 14:56:50', '2013-02-20 07:57:28', 3),
(7, 'admin', '$2a$08$ts/.u.Jccox8zJE8RcmsK.Aa.INhSeRNl8sECFpecQxLejJ9.ob3u', 'water7wall@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '::1', '2013-02-24 14:38:00', '2013-01-28 06:27:18', '2013-02-24 07:38:00', 2),
(8, 'amir', '$2a$08$5w4F45hhPtmsOAsbNyWAq.cKiJiKg4CxX3zabkTu2.aguJ1zEXGBu', 'cak.azharr@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2013-02-19 15:35:17', '2013-02-19 15:31:36', '2013-02-19 08:35:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 3, NULL, NULL),
(2, 4, NULL, NULL),
(3, 5, NULL, NULL),
(4, 8, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
